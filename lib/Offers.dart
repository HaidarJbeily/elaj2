import 'package:flutter/material.dart';
import 'package:elag_app/MyDrawer.dart';
import 'package:elag_app/Model/Advert.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'package:elag_app/connectionmanager.dart';
import 'package:url_launcher/url_launcher.dart';
import 'app_localizations.dart';

class Offer extends StatelessWidget {
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    _scaffoldKey = new GlobalKey();
    List<BoxShadow> listShadow = new List();
    listShadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.1,
    ));

    List<Color> colors = new List();
    colors.add(repo.purple3);
    colors.add(repo.purple2);
    colors.add(repo.purple1);

   
    return Theme(
      data: new ThemeData(
        splashColor: repo.purple3,
        primaryColor: repo.purple2,
        accentColor: repo.purple2,
        inputDecorationTheme: new InputDecorationTheme(
          labelStyle: new TextStyle(
            fontSize: 15.0,
            fontFamily: 'Hacen',
          ),
        ),
      ),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[200],
        drawer: new Opacity(
          opacity: 0.92,
          child: MyDrawer(),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backgroundHomePage.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: width,
              height: 82.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                gradient:
                    LinearGradient(colors: colors, tileMode: TileMode.mirror),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 25.0),
                      child: Text(
                        AppLocalizations.of(context).translate('offers'),
                        style: TextStyle(
                          fontSize: 26.5,
                          //fontWeight: FontWeight.w900,
                          fontFamily: 'Hacen',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
           Positioned(
              
              
              top: Localizations.localeOf(context).languageCode =='ar'?27.0:27.0,
              left: Localizations.localeOf(context).languageCode =='ar'?5.0:null,
              right: Localizations.localeOf(context).languageCode =='ar'?null:5.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: Colors.white,
                        size: 26.0,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ),
              ),
            ),
            Positioned(
             top: Localizations.localeOf(context).languageCode =='en'?42.0:42.0,
              left: Localizations.localeOf(context).languageCode =='en'?16.0:null,
              right: Localizations.localeOf(context).languageCode =='en'?null:18.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: Image.asset(
                  'images/drawerIcon.png',
                  height: 18.0,
                  width: 18.0,
                ),
              ),
            ),

            /////////////////////
            new OfferHome(),
          ],
        ),
      ),
    );
  }
}

class OfferHome extends StatefulWidget {
  @override
  _OfferHomeState createState() => _OfferHomeState();
}

class _OfferHomeState extends State<OfferHome> {
  int page;
  int currentPage;
  List<Advert> offers;

  bool isLoading;

  ScrollController scrollController;

  
   @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  
    offers = List();
    page = 0;
    currentPage = 0;
    isLoading = false;
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 25 &&
          offers.length % 10 == 0 &&
          !isLoading &&
          page == currentPage) {
        loadData();
      }
    });
    loadData();
  }

  void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });
      print('$page , $currentPage');
      page++;
      getOffers('page=${page}',Localizations.localeOf(context).languageCode).then((response) {
        if (response.statusCode == 200) {
          offers.addAll(response.object);
//          currentPage++;
          currentPage = (offers.length / 10).ceil();
        }
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    createOfferTile(Advert ads) => Padding(
          padding:
              EdgeInsets.only(top: 12.0, bottom: 12.0, left: 20.0, right: 20.0),
          child: GestureDetector(
            onTap: () async {
              print('${ads.url}  ${ads.id}');
              if (await canLaunch(ads.url)) {
                launch(ads.url);
              } else {
                print('cannot open');
              }
            },
            child: Container(
              height: width / 2.0,
              width: width,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                    color: Colors.black54,
                    width: 0.5,
                    style: BorderStyle.solid),
                color: Colors.black38,
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage('$baseUrl/storage/${ads.image}'),
                ),
              ),
            ),
          ),
        );

    return Padding(
      padding: EdgeInsets.only(top: 88.0, bottom: 12.0),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: 12.0, bottom: 12.0, left: 20.0, right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  height: width / 2.0,
                  width: width,
                  decoration: new BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                        color: Colors.black54,
                        width: 0.5,
                        style: BorderStyle.solid),
                    color: Colors.black38,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('images/advert2.jpg'),
                    ),
                  ),
                ),
              ),
            ),
            offers.length > 0
                ? Column(
                    children:
                        offers.map((ads) => createOfferTile(ads)).toList(),
                  )
                : isLoading
                    ? Container()
                    : Center(
                        child: Text(AppLocalizations.of(context)
                            .translate('no_results_found')),
                      ),
            isLoading
                ? Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate('loading...'),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.black87,
                            ),
                          )),
                    ],
                  )
                : Container(),
          ],
        ),
      ),
    );
//                      } else
//                        return Center(
//                          child: Text('حدث خطأ ما'),
//                        );
//                    } else
//                      return Center(
//                        child: Text('no data'),
//                      );
//                  } else {
//                    return Column(
//                      mainAxisSize: MainAxisSize.max,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: CircularProgressIndicator(),
//                        ),
//                        Center(
//                          child: Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Text(
//                                'جارٍ التحميل .. ',
//                                style: TextStyle(
//                                  fontSize: 18.0,
//                                  color: Colors.black87,
//                                ),
//                              )),
//                        )
//                      ],
//                    );
//                  }
//                }),
//          ),
//        ],
//      )),
//    );
  }
}
