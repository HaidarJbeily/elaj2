

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_svg/flutter_svg.dart';

//import 'package:url_launcher/url_launcher.dart';
import 'package:elag_app/Offers.dart';
import 'package:elag_app/MyDrawer.dart';
import 'package:elag_app/MedicalAdverts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:elag_app/PublicSearch.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'package:elag_app/Dialog.dart';
import 'connectionmanager.dart';

import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'app_localizations.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void getData() async{
    var response1 = await getMajorSpecialties(Localizations.localeOf(context).languageCode);
    if (response1.statusCode == 200) {
      majorSpecialties = response1.object;
    }

    var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
    if (response2.statusCode == 200) {
      Specialties = response2.object;
    }

    var response3 = await getCountries(Localizations.localeOf(context).languageCode);
    if (response3.statusCode == 200) {
      Countries = response3.object;
    }

    var response4 = await getLocations(Localizations.localeOf(context).languageCode);
    if (response4.statusCode == 200) {
      Locations = response4.object;
    }

    var response5 = await getAboutApp(Localizations.localeOf(context).languageCode);
    if (response5.statusCode == 200) {
      aboutApp = response5.object;
    }
  }
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getData();
  }
  @override
  void dispose() { 
    super.dispose();
  }

   
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Center(
         child: Localizations.localeOf(context).languageCode == 'en' ?Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 50, 0),
          child: Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ):
          Padding(
            padding:EdgeInsets.fromLTRB(50, 0, 0, 0) ,
            child:Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ),

        ),

        backgroundColor: repo.blue1,
       ),
  
      drawer: Opacity(
        opacity: 0.92,
        child: MyDrawer(CountryCode: Localizations.localeOf(context).languageCode,check: 1,),
      ),
      backgroundColor: Colors.grey[200],
      body: MyHome(CountryCode: Localizations.localeOf(context).languageCode,),
    );
  }
}

class MyHome extends StatelessWidget {
  String CountryCode;
  MyHome({this.CountryCode});
  Future<void> requestAgain() async {
    if (Countries.length == 0) {
      var response = await getCountries(CountryCode);
      if (response.statusCode == 200) {
        Countries = response.object;
      }
    }

    if (Locations.length == 0) {
      var response = await getLocations(CountryCode);
      if (response.statusCode == 200) {
        Locations = response.object;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print(this.CountryCode);
    double width, height;
    double aspectRatio = MediaQuery.of(context).size.aspectRatio;
    if (aspectRatio >= 1.0) {
      width = MediaQuery.of(context).size.width;
      height = width * aspectRatio - AppBar().preferredSize.height - 25;
    } else {
      width = MediaQuery.of(context).size.width;
      height = width / aspectRatio - AppBar().preferredSize.height - 25;
    }
    print('Width $width');
    print('Height $height');

    Future<void> _SpecialtyPopUp(int id, List<Specialty> spec, String title,
        String imgPath, Color col, String defaultImage) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          title = title == null ? "Not":title;
          title = title == null ? "Not":title;
          title = title == null ? "Not":title;
          return ProfileDialog(id, spec, title, imgPath, col, defaultImage);
        },
      );
    }

    Future<void> _LocationPopUp(
        List<Location> Loc, String title, String imgPath, Color col) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new LocationDialog(Loc, title, imgPath, col);
        },
      );
    }

    List<BoxShadow> list = new List();
    list.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.8,
    ));

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/backgroundHomePage.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SingleChildScrollView(
          child: Container(
              width: double.infinity,
              height: height,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(flex: 30, child: MedicalAdverts()),
                  Expanded(
                      flex: 10,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: SearchBar()),
                            GestureDetector(
                              onTap: () async {
                                print('cities');
                                // request again
                                requestAgain();

                                if (majorSpecialties.length == 0) {
                                  var response1 = await getMajorSpecialties(CountryCode);
                                  if (response1.statusCode == 200) {
                                    majorSpecialties = response1.object;
                                  }
                                }

                                if (Specialties.length == 0) {
                                  var response2 = await getSpecialties(CountryCode);
                                  if (response2.statusCode == 200) {
                                    Specialties = response2.object;
                                  }
                                }

                                // end request again
                                _LocationPopUp(
                                    Locations,
                                    AppLocalizations.of(context)
                                        .translate('cities'),
                                    'images/cityPopUp.png',
                                    repo.yellow1);
                              },
                              child: Container(
                                width: 100.0,
                                height: 40.0,
                                decoration: BoxDecoration(
//                                  color: Colors.orange,
                                    gradient: LinearGradient(colors: [
                                      repo.yellow1,
                                      repo.yellow2,
//                                      repo.yellow3
                                    ]),
                                    borderRadius: BorderRadius.circular(4.0)),
                                child: Center(
                                    child: Text(
                                  AppLocalizations.of(context)
                                      .translate('select_city'),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )),
                              ),
                            )
                          ],
                        ),
                      )),
                  Expanded(
                      flex: 40,
                      child: Container(
//                    color: Colors.blue,
                        padding: EdgeInsets.only(
                            top: 8.0, left: 35.0, right: 35.0, bottom: 12.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/doctors-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    List<Specialty> spec = new List();
                                    int _id = 1;
                                    requestAgain();
                                    for (int i = 0;
                                        i < Specialties.length;
                                        i++) {
                                      if (Specialties[i].parentId == _id) {
                                        spec.add(Specialties[i]);
                                      }
                                    }

                                    _SpecialtyPopUp(
                                        1,
                                        spec,
                                        AppLocalizations.of(context)
                                            .translate('doctors'),
                                        'images/doctorPopUp.png',
                                        repo.blue1,
                                        'images/doctorsDefault.png');
                                  },
                                ),
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/dentists-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    print('dentists');

                                    // request again
                                    requestAgain();
                                    // end request again
                                    List<Specialty> spec = new List();
                                    int _id = 125;
                                    for (int i = 0;
                                        i < Specialties.length;
                                        i++) {
                                      if (Specialties[i].parentId == _id) {
                                        spec.add(Specialties[i]);
                                      }
                                    }
                                    _SpecialtyPopUp(
                                        125,
                                        spec,
                                        AppLocalizations.of(context)
                                            .translate('dentists'),
                                        'images/dentistPopUp.png',
                                        repo.tooth1,
                                        'images/dentistMenu.png');
                                  },
                                ),
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/pharmacies-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    print('pharmacies');

                                    // request again
                                    requestAgain();
                                    // end request again
                                    List<Specialty> spec = new List();
                                    int _id = 4;
                                    for (int i = 0;
                                        i < Specialties.length;
                                        i++) {
                                      if (Specialties[i].parentId == _id) {
                                        spec.add(Specialties[i]);
                                      }
                                    }
                                    _SpecialtyPopUp(
                                        4,
                                        spec,
                                        AppLocalizations.of(context)
                                            .translate('pharmacies'),
                                        'images/pharmaciesPopUp.png',
                                        repo.green1,
                                        'images/pharmaciesDefault.png');
                                  },
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20.0),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/offers-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    print('Offers');
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => Offer()));
//                                // request again
//                                requestAgain();
//                                // end request again
//                                List<Specialty> spec = new List();
//                                int _id = 3;
//                                for (int i = 0; i < Specialties.length; i++) {
//                                  if (Specialties[i].parentId == _id) {
//                                    spec.add(Specialties[i]);
//                                  }
//                                }
//                                _SpecialtyPopUp(3, spec, 'الأكاديمية الطبية',
//                                    'images/academicPopUp.png', repo.purple1);
                                  },
                                ),
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/medCenters-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    print('medicalCenter');
                                    // request again
                                    requestAgain();
                                    // end request again
                                    List<Specialty> spec = new List();
                                    int _id = 2;
                                    for (int i = 0;
                                        i < Specialties.length;
                                        i++) {
                                      if (Specialties[i].parentId == _id) {
                                        spec.add(Specialties[i]);
                                      }
                                    }
                                    _SpecialtyPopUp(
                                        2,
                                        spec,
                                        AppLocalizations.of(context)
                                            .translate('medical_centers'),
                                        'images/medicalCenterPopUp.png',
                                        repo.red1,
                                        'images/centersDefault.png');
                                  },
                                ),
                                new GestureDetector(
                                  child: Container(
                                    width: width / 5.2,
                                    height: width / 4.0,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'images/homescreen/medServices-${this.CountryCode}.png'),
                                            fit: BoxFit.contain)),
                                  ),
                                  onTap: () async {
                                    print('services');

                                    // request again
                                    requestAgain();
                                    // end request again
                                    List<Specialty> spec = new List();
                                    int _id = 97;
                                    for (int i = 0;
                                        i < Specialties.length;
                                        i++) {
                                      if (Specialties[i].parentId == _id) {
                                        spec.add(Specialties[i]);
                                      }
                                    }
                                    _SpecialtyPopUp(
                                        97,
                                        spec,
                                        AppLocalizations.of(context).translate(
                                            'medical_tourism_services'),
                                        'images/servicesPopUp.png',
                                        repo.purple1,
                                        'images/servicesMenu.png');
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),
                  Expanded(
                    flex: 5,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            //Navigator.of(context).pop();
                            // Navigator.of(context).push(
                            //   MaterialPageRoute(builder: (context) => Home()));
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                titlePadding: EdgeInsets.all(2.0),
                                title: Center(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate('about_us'),
                                    style: TextStyle(
                                      color: repo.blue1,
                                      fontSize: 30.0,
                                      fontFamily: 'Hacen',
                                      fontWeight: FontWeight.w900,
                                    ),
                                  ),
                                ),
                                contentPadding: EdgeInsets.only(
                                    top: 0.0, left: 0.0, right: 0.0),
                                content: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.98,
                                  height:
                                      MediaQuery.of(context).size.height * 0.94,
                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: <Widget>[
                                        aboutApp == null
                                            ? Text('')
                                            : HtmlWidget(
                                                aboutApp,
                                                onTapUrl: (url) async {
                                                  if (await canLaunch(url)) {
                                                    print('cannot open $url');
                                                    await launch(url);
                                                  } else {
                                                    print('cannot open $url');
                                                  }
                                                },
                                                textStyle: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16.0,
                                                  fontFamily: 'Hacen',
                                                  fontWeight: FontWeight.w900,
                                                ),
//                                            renderNewlines: true,
                                              ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 2.0, right: 2.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () async {
                                                  launch(
                                                      'http://www.tinawiworld.com');
                                                },
                                                child: Container(
                                                  width: 100.0,
                                                  height: 101.0,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                'owner_company'),
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                          fontFamily: 'Hacen',
                                                          fontSize: 10.0,
                                                        ),
                                                      ),
                                                      Image.asset(
                                                        'images/dark_footer.png',
                                                        width: 50.0,
                                                        height: 50.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () async {
                                                  launch(
                                                      'http://www.zoplus.net');
                                                },
                                                child: Container(
                                                  width: 100.0,
                                                  height: 101.0,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                'programming_company'),
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                          fontFamily: 'Hacen',
                                                          fontSize: 10.0,
                                                        ),
                                                      ),
                                                      Image.asset(
                                                        'images/footer.png',
                                                        width: 50.0,
                                                        height: 50.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.all(4.0),
                            height: 35.0,
                            child: Image(
                              image: AssetImage('images/dark_footer.png'),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
//                        Padding(
//                          padding: const EdgeInsets.only(left:8.0,right: 8.0),
//                          child: Container(width: 1.0,height: 35.0,color: Colors.grey,),
//                        ),
//                        GestureDetector(
//                          onTap: () async {
//                            String url = 'http://www.zoplus.net';
//                            if (await canLaunch(url)) {
//                              await launch(
//                                url,
//                              );
//                            } else {
//                              print('cannot open');
//                            }
//                          },
//                          child: Container(
//                            padding: EdgeInsets.all(4.0),
//                            height: 35.0,
//                            child: Image(
//                              image: AssetImage('images/footer.png'),
//                              fit: BoxFit.contain,
//                            ),
//                          ),
//                        ),
                      ],
                    ),
                  )
                ],
              )),
        ),
//        new Positioned(
//            bottom: 4.0,
//            child: Padding(
//              padding: EdgeInsets.only(top: 8.0),
//              child: Container(
//                height: 25.0,
//                width: width,
//                decoration: BoxDecoration(
//                    image: DecorationImage(
//                  image: AssetImage('images/footer.png'),
//                  fit: BoxFit.contain,
//                )),
//              ),
//            )),
      ],
    );
  }
}

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  TextEditingController _controllerSearch;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
     _controllerSearch = new TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    return new MediaQuery.removePadding(
      context: context,
      child: Form(
        child: Theme(
          data: new ThemeData(
            primaryColor: repo.blue2,
            inputDecorationTheme: new InputDecorationTheme(
              labelStyle: new TextStyle(
                fontSize: 20.0,
                fontFamily: 'Hacen',
              ),
            ),
          ),
          child: Container(
            padding: const EdgeInsets.only(
              left: 30.0,
              right: 30.0,
            ),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new TextFormField(
                  decoration: new InputDecoration(
                    labelText: AppLocalizations.of(context).translate("search"),
                    icon: Icon(
                      Icons.search,
                    ),
//                    fillColor: Colors.orange,
//                    filled: true,
                    contentPadding: EdgeInsets.only(top: 4.0, bottom: 4.0),
                  ),
                  keyboardType: TextInputType.text ?? '',
                  controller: _controllerSearch,
                  onEditingComplete: () {
                    if (_controllerSearch.text != null &&
                        _controllerSearch.text.length > 0) {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PublicSearch(
                              null, _controllerSearch.text.trim())));
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      removeBottom: true,
      removeTop: true,
    );
  }
}
