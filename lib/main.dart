import 'package:elag_app/PublicSearch.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app_localizations.dart';
import 'package:elag_app/HomePage.dart';
import 'package:elag_app/Launcher.dart';
import 'MedicalConsultationFormPage.dart';
import 'JoinUsFormPage.dart';
import 'language_constants.dart';
import 'NotificationScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description: 'This channel is used for important notifications.', // description
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  await FirebaseMessaging.instance.subscribeToTopic('System');
  await FirebaseMessaging.instance.subscribeToTopic('system');

    

  runApp(MyApp());
}


class MyApp extends StatefulWidget {
 

  
  @override
  _MyAppState createState() => _MyAppState();
  static void setLocale(BuildContext context, Locale newLocale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(newLocale);
  }
  static _MyAppState of(BuildContext context) => context.findAncestorStateOfType<_MyAppState>();
}


class _MyAppState extends State<MyApp> {
  
  Locale _locale ;
  
  void setLocale(Locale value) {
    setState(() {
      print(_locale);
      _locale = value;
    });
  }
  @override
  void initState() {
    getLocale().then((locale) {
      setState(() {
        this._locale = locale;
      });
    });
    // TODO: implement initState
    super.initState();
     FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channelDescription:channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ));
            }});
            FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        showDialog(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: Text(notification.title),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [Text(notification.body)],
                  ),
                ),
              );
            });
      }
    });
       
    
  }
  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        this._locale = locale;
      });
    });
    super.didChangeDependencies();
  }

    @override
  Widget build(BuildContext context) {
    return MaterialApp(
     
        title: 'Elag',
        locale: _locale,
        theme: ThemeData(
          splashColor: Color.fromARGB(50, 20, 180, 250),
        ),
        routes: <String, WidgetBuilder>{
          '/HomePage': (BuildContext context) => new HomePage(),
          '/MedicalConsultationFormPage': (BuildContext context) =>
              new MedicalConsultationFormPage(),
          '/SubscribeFormPage': (BuildContext context) =>
              new SubscribeFormPage(),
              '/PublicSearch':(BuildContext context) =>
              new PublicSearch(null, null),
              '/Notification':(BuildContext context) =>
              new NotifictionScreen(),
        },
        localizationsDelegates: [
          AppLocalizations.delegate,
          //GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'), // English
          const Locale('ar', 'KSA'), // Arabic
          ],
        
        // Returns a locale which will be used by the app
        localeResolutionCallback: (locale, supportedLocales) {
          // Check if the current device locale is supported
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode) {
              return supportedLocale;
            }
          }
          // If the locale of the device is not supported, use the first one
          // from the list (English, in this case).
          return supportedLocales.first;
        },
        home: Launcher());
  }
}
