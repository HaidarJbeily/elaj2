import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'Profile.dart';
import 'package:elag_app/Model/MedicalProfile.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;

import 'connectionmanager.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'app_localizations.dart';

class PublicSearch extends StatefulWidget {
  
   Location _locationSelected;
  final String nameQuery;

  PublicSearch(this._locationSelected, this.nameQuery);

  @override
  _PublicSearchState createState() => _PublicSearchState();
}

class _PublicSearchState extends State<PublicSearch> {
  
  @override
  void initState() { 
    super.initState();
    
  }
  
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: repo.blue2,
        title: Center(
          child: Localizations.localeOf(context).languageCode =='en'? Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 50, 0),
          child: Text(
            AppLocalizations.of(context).translate('search'),
            style: new TextStyle(
              fontSize: 26.0,
              color: Colors.white,
              fontFamily: 'Hacen',
            ),
          ),): Padding(
            padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
          child: Text(
            AppLocalizations.of(context).translate('search'),
            style: new TextStyle(
              fontSize: 26.0,
              color: Colors.white,
              fontFamily: 'Hacen',
            ),
          ),),

        ),
       
      ),
      // drawer: new Opacity(
      //   opacity: 0.92,
      //   child: MyDrawer(),
      // ),
      body: Padding(
        padding: EdgeInsets.only(top: 6.0),
        child: HomePublicSearch(widget._locationSelected, widget.nameQuery),
      ),
    );
  }
}



class HomePublicSearch extends StatefulWidget {
  Location _locationSelected;
  final String _nameQuery;

  HomePublicSearch(this._locationSelected, this._nameQuery);

  @override
  _HomePublicSearchState createState() => _HomePublicSearchState();
}

class _HomePublicSearchState extends State<HomePublicSearch> {
  Location _locationSelected;
  Specialty _majorSpecialtySelected;
  Specialty _SpecialtySelected;

  List<Specialty> spec;
  List<Specialty> majorSpec;
  List<Location> loc;
  List<MedicalProfile> profiles;

  bool isLoading;

  int page, currentPage;

  ScrollController scrollController;

  String _queryMajorSpecialty = '';
  String _querySpecialty = '';
  String _queryLocation = '';
  String _queryName = '';
  String _query = '';

  TextEditingController _controllerSearch;
  List<Color> colorProfile;

  void buildQuery() {
    print('building Query');
    _query = '';
    if (_queryMajorSpecialty != '') {
      _query = '$_query$_queryMajorSpecialty&';
    }
    if (_queryLocation != '') {
      _query = '$_query$_queryLocation&';
    }
    if (_querySpecialty != '') {
      _query = '$_query$_querySpecialty&';
    }
    if (_queryName != '') {
      _query = '$_query$_queryName&';
    }
  }

  void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });
      print('$page , $currentPage');

      page++;
      getMedicalProfiles('${_query}page=${page}',Localizations.localeOf(context).languageCode).then((response) {
        if (response.statusCode == 200) {
          profiles.addAll(response.object);
          currentPage++;
        }
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
  } // init State
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    profiles = List();
    isLoading = false;
    page = 0;
    currentPage = 0;

    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 25 &&
          profiles.length % 10 == 0 &&
          !isLoading) {
        print('shouldLoad');
        loadData();
      }
    });

    _controllerSearch = new TextEditingController();
    majorSpec = new List();
    majorSpec.add(new Specialty(
        -1,
        AppLocalizations.of(context).translate('choose_general_speciality'),
        1));
    for (int i = 0; i < majorSpecialties.length; i++) {
      majorSpec.add(majorSpecialties[i]);
    }
    _majorSpecialtySelected = majorSpec[0];

    //spec = Specialties;
    spec = new List();
    spec.add(new Specialty(
        -1,
        AppLocalizations.of(context).translate('choose_precise_speciality'),
        -1));
    _SpecialtySelected = spec[0];

    if (widget._locationSelected != null) {
      _locationSelected = widget._locationSelected;
      if (_locationSelected.parentId == null) {
        _queryLocation = 'location=${_locationSelected.id}';
      } else {
        _queryLocation =
            'location=${_locationSelected.id},${_locationSelected.parentId}';
      }
      // set initial query
//      _query = _queryLocation;
    }

    if (widget._nameQuery != null) {
      _queryName = 'query=${widget._nameQuery}';
      _controllerSearch.text = widget._nameQuery;
//      if (_query.length > 0) {
//        _query += '&' + _queryName;
//      } else {
//        _query = _queryName;
//      }
    }

    loc = new List();
    loc.add(new Location(
        -1,
        AppLocalizations.of(context).translate('choose_city'),
        '-1',
        '-1',
        -1,
        -1));
    _locationSelected = widget._locationSelected ?? loc[0];
    if (Countries != null) {
      for (int i = 0; i < Countries.length; i++) {
        loc.add(Countries[i]);
        for (int j = 0; j < Locations.length; j++) {
          if (Locations[j].parentId == Countries[i].id) {
            loc.add(Locations[j]);
          }
        }
      }
    }
    buildQuery();
    loadData();
  }
  List<String> getLeftRightImg(int id) {
    List<String> img = new List();
    if (id == 1) {
      img.add('images/doctorSearchLeft.png');
      img.add('images/doctorSearchRight.png');
    } else if (id == 2) {
      img.add('images/centerSearchLeft.png');
      img.add('images/centerSearchRight.png');
    } else if (id == 3) {
      img.add('images/academicSearchLeft.png');
      img.add('images/academicSearchRight.png');
    } else if (id == 4) {
      img.add('images/pharamSearchLeft.png');
      img.add('images/pharamSearchRight.png');
    } else if (id == 125) {
      img.add('images/toothSearchLeft.png');
      img.add('images/toothSearchRight.png');
    } else if (id == 97) {
      img.add('images/servicesMedicalSearchLeft.png');
      img.add('images/servicesMedicalSearchRight.png');
    } else {
      // Exception
      img.add('images/doctorSearchLeft.png');
      img.add('images/doctorSearchRight.png');
    }
    return img;
  }

  List<Color> getColorProfile(int id) {
    List<Color> colors = new List();
    if (id == 1) {
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    } else if (id == 2) {
      colors.add(repo.red3);
      colors.add(repo.red2);
      colors.add(repo.red1);
    } else if (id == 3) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else if (id == 4) {
      colors.add(repo.green3);
      colors.add(repo.green2);
      colors.add(repo.green1);
    } else if (id == 125) {
      colors.add(repo.tooth3);
      colors.add(repo.tooth2);
      colors.add(repo.tooth1);
    } else if (id == 97) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else {
      // Exception
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    }
    return colors;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    List<BoxShadow> listShadow = new List();
    listShadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.1,
    ));
    var searchTextStyle = new TextStyle(
      color: Colors.black54,
      fontSize: 12.0,
      fontFamily: 'Hacen',
    );

    createTileProfile(
            MedicalProfile profile, List<Color> col, List<String> img) =>
        GestureDetector(
          onTap: () async {
            if (profile.level == 'one') {
              Scaffold.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                content: Text(
                  '${profile.name} ${AppLocalizations.of(context).translate('is_not_subscribed_to_the_service')}',
                  style: TextStyle(fontFamily: 'Hacen'),
                  textAlign: TextAlign.center,
                ),
              ));
            } else if (profile.level == 'two') {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Profile(profile)));
            } else if (profile.level == 'three') {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Profile(profile)));
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                content: Text(
                  AppLocalizations.of(context).translate('some_error_occurred'),
                  style: TextStyle(fontFamily: 'Hacen'),
                  textAlign: TextAlign.center,
                ),
              ));
            }
          },
          child: Padding(
            padding:
                EdgeInsets.only(left: 15.0, right: 15.0, bottom: 6.0, top: 6.0),
            child: Container(
              height: 110.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                        color: col[1], style: BorderStyle.solid, width: 6.0)),
              ),
              child: Stack(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: CircleAvatar(
                          radius: 40.0, //width / 8.8
                          backgroundColor: col[1],
                          child: CircleAvatar(
                            radius: 38.5, //width / 9.1
                            backgroundColor: Colors.white,
                            backgroundImage: profile.logo == null
                                ? AssetImage(
                                    img[1],
                                  )
                                : NetworkImage(
                                    '$baseUrl/storage/${profile.logo}'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 8.0, top: 14.0, bottom: 6.0, right: 8.0),
                        child: Container(
                          width: width / 4.2,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(
                                image: AssetImage(img[0]),
                                fit: BoxFit.contain,
                              )),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: AutoSizeText(
                      profile.name != null ? profile.name.trim() : 'Not Translated ',
                      maxLines: 1,
                      minFontSize: 12.0,
                      maxFontSize: 18.0,
                      textAlign: TextAlign.end,
                      textDirection:Localizations.localeOf(context).languageCode =='ar'?TextDirection.ltr:TextDirection.rtl,
                      style: TextStyle(
//                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Hacen',
                        color: col[2],
                      ),
                      //textDirection: TextDirection.rtl,
                    ),
                   top: 10.0,
                  right:Localizations.localeOf(context).languageCode =='ar'?100.0:null,
                  left:Localizations.localeOf(context).languageCode =='ar'?null:100.0, //width / 3.6//width / 3.6
                  ),
                  Positioned(
                    child: Row(
                      children: <Widget>[
                        Expanded(
//                          height: 55.0,
//                          width: width / 3.2,
                          child: Align(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: profile.specialty != null
                                        ? Row(
                                            children: <Widget>[
                                              SvgPicture.asset(
                                                'images/specialistProfile.svg',
                                                width: 20.0,
                                                height: 20.0,
                                                color: col[2],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    right: 2.0, left: 2.0),
                                              ),
                                              Expanded(
                                                child:  AutoSizeText(
                                                        profile.specialty.name
                                                            ??'Not translated',
                                                        maxLines: 2,
                                                        minFontSize: 4.0,
                                                        textAlign:
                                                            TextAlign.start,
                                                        textDirection:
                                                           Localizations.localeOf(context).languageCode =='en'?TextDirection.ltr:TextDirection.rtl,
                                                        style: searchTextStyle,
                                                      ),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                  ),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: profile.location != null
                                        ? Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              Image.asset(
                                                'images/cityPopUp.png',
                                                width: 20.0,
                                                height: 20.0,
                                                color: col[2],
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    right: 2.0, left: 2.0),
                                              ),
                                              Expanded(
                                                child: profile.location.name !=
                                                        null
                                                    ? AutoSizeText(
                                                        profile.location.name
                                                            .trim(),
                                                        maxLines: 2,
                                                        minFontSize: 4.0,
                                                        textAlign:
                                                            TextAlign.start,
                                                        textDirection:
                                                            Localizations.localeOf(context).languageCode =='en'?TextDirection.ltr:TextDirection.rtl,
                                                        style: searchTextStyle,
                                                      )
                                                    : Container(),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 1.0, right: 1.0),
                        ),
                        Expanded(
//                          height: 55.0,
//                          width: width / 3.2,
                          child: Align(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Expanded(
                                  child: Align(
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: <Widget>[
                                          Image.asset(
                                            'images/mobile.png',
                                            width: 20.0,
                                            height: 20.0,
                                            color: col[2],
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                right: 2.0, left: 2.0),
                                          ),
                                          Expanded(
                                            child: profile.mobile != null
                                                ? AutoSizeText(
                                                    profile.mobile.trim(),
                                                    maxLines: 2,
                                                    minFontSize: 4.0,
                                                    textAlign: TextAlign.start,
                                                    textDirection:
                                                        Localizations.localeOf(context).languageCode =='en'?TextDirection.ltr:TextDirection.rtl,
                                                    style: searchTextStyle,
                                                  )
                                                : Container(
                                                    child: Text('-'),
                                                  ),
                                          ),
                                        ],
                                      )),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Row(
                                      children: <Widget>[
                                        SvgPicture.asset(
                                          'images/teleProfiles.svg',
                                          width: 20.0,
                                          height: 20.0,
                                          color: col[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 2.0, left: 2.0),
                                        ),
                                        Expanded(
                                          child: profile.phone != null
                                              ? AutoSizeText(
                                                  profile.phone.trim(),
                                                  maxLines: 2,
                                                  minFontSize: 4.0,
                                                  textAlign: TextAlign.start,
                                                  textDirection:
                                                      Localizations.localeOf(context).languageCode =='en'?TextDirection.ltr:TextDirection.rtl,
                                                  style: searchTextStyle,
                                                )
                                              : Container(
                                                  child: Text('-'),
                                                ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    top: 45.0,
                    right: Localizations.localeOf(context).languageCode == 'ar'? 100.0:4.0,
                    left:  Localizations.localeOf(context).languageCode == 'ar'? 4.0:100.0,
                    bottom: 4.0,
//                    width: width / 1.6,
                  ),
                ],
              ),
            ),
          ),
        );

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/backgroundHomePage.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: <Widget>[
              Container(
                width: width,
                height: 65.0,
                decoration: BoxDecoration(
                  boxShadow: listShadow,
                  color: Colors.white,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      height: 65.0,
                      width: width / 1.57,
                      color: Colors.white,
                      child: Container(
                        //padding: const EdgeInsets.only(left: 2.0, right: 5.0),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(5.0),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 12.0, left: 12.0),
                              child: new DropdownButton(
                                  isExpanded: true,
                                  style: TextStyle(
                                    fontSize: 18.5,
                                    fontFamily: 'Hacen',
                                    color: Colors.black,
                                  ),
                                  value: _majorSpecialtySelected,
                                  items: majorSpec
                                      .map((spec) => DropdownMenuItem(
                                            value: spec,
                                            child: Container(
                                                child: AutoSizeText(spec.name??'')),
                                          ))
                                      .toList(),
                                  onChanged: (val) {
                                    setState(() {
                                      _majorSpecialtySelected = val;
                                      if (_majorSpecialtySelected.id != -1) {
                                        spec = new List();
                                        for (int i = 0;
                                            i < Specialties.length;
                                            i++) {
                                          if (Specialties[i].parentId ==
                                              _majorSpecialtySelected.id) {
                                            spec.add(Specialties[i]);
                                          }
                                        }
                                        spec.insert(
                                            0,
                                            new Specialty(
                                                -1,
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        'choose_precise_speciality'),
                                                -1));
                                        _SpecialtySelected = spec[0];
                                      } else {
                                        spec = new List();
                                        spec.add(new Specialty(
                                            -1,
                                            AppLocalizations.of(context)
                                                .translate(
                                                    'choose_precise_speciality'),
                                            -1));
                                        _SpecialtySelected = spec[0];
                                      }

                                      if (_majorSpecialtySelected.id != -1) {
                                        _queryMajorSpecialty =
                                            'specialty=${_majorSpecialtySelected.id}';
                                      } else {
                                        _queryMajorSpecialty = '';
                                        _querySpecialty = '';
                                      }
                                    });
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 65.0,
                      width: width / 2.77,
                      color: Colors.white,
                      child: Container(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(5.0),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right: 6.0, left: 6.0, bottom: 5.0),
                              child: new DropdownButton(
                                  isExpanded: true,
                                  //hint: Text('city'),
                                  style: TextStyle(
                                    fontSize: 14.5,
                                    fontFamily: 'Hacen',
                                    color: Colors.black,
                                  ),
                                  value: _locationSelected,
                                  items: loc
                                      .map((loc) => DropdownMenuItem(
                                            value: loc,
                                            child: Container(
                                              //width: width / 3.8,
                                              child: Text(loc.name),
                                            ),
                                          ))
                                      .toList(),
                                  onChanged: (val) {
                                    setState(() {
                                      _locationSelected = val;
                                      if (_locationSelected.id != -1) {
                                        if (_locationSelected.parentId !=
                                            null) {
                                          _queryLocation =
                                              'location=${_locationSelected.id},${_locationSelected.parentId}';
                                        } else {
                                          _queryLocation =
                                              'location=${_locationSelected.id}';
                                        }
                                      } else {
                                        _queryLocation = '';
                                      }
                                      print(_queryLocation);
                                    });
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //////////////////////////////////////////////////////////

              Container(
                  width: width,
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.only(right: 12.0, left: 12.0, top: 5.0),
                    child: new DropdownButton(
                        isExpanded: true,
                        style: TextStyle(
                          fontSize: 18.5,
                          fontFamily: 'Hacen',
                          color: Colors.black,
                        ),
                        value: _SpecialtySelected,
                        items: spec
                            .map((sp) => DropdownMenuItem(
                                value: sp,
                                child: Container(
                                  width: width / 1.143,
                                  child: Text(
                                    sp.name,
                                  ),
                                )))
                            .toList(),
                        onChanged: (val) {
                          setState(() {
                            _SpecialtySelected = val;
                            if (_SpecialtySelected.id != -1) {
                              _querySpecialty =
                                  'specialty=${_SpecialtySelected.id},${_SpecialtySelected.parentId}';
                            } else {
                              _querySpecialty = '';
                            }
                          });
                        }),
                  )),

              Container(
                width: width,
                height: 85.0,
                color: Colors.white,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      height: 85.0,
                      width: width / 1.57,
                      color: Colors.white,
                      child: Container(
                        
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                right: 12.0,
                                left: 12.0,
                              ),
                              child: new TextFormField(
                                decoration: new InputDecoration(
                                  labelText: AppLocalizations.of(context)
                                      .translate('name..'),
                                ),
                                keyboardType: TextInputType.text,
                                controller: _controllerSearch,
                              
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 85.0,
                      width: width / 2.77,
                      color: Colors.white,
                      child: Container(
                        padding: const EdgeInsets.only(
                            left: 4.0, right: 8.0, bottom: 5.0),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(5.0),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  right: 15.0,
                                  left: 15.0,
                                  bottom: 6.0,
                                  top: 15.0),
                              child: new MaterialButton(
                                height: 35.0,
                                minWidth: 140.0,
                                onPressed: () async {
                                  setState(() {
                                    _controllerSearch.text =
                                        _controllerSearch.text.trim();
                                    if (_controllerSearch.text != null &&
                                        _controllerSearch.text.length > 0) {
                                      _queryName =
                                          'query=${_controllerSearch.text}';
                                    } else {
                                      _queryName = '';
                                    }
                                    buildQuery();
                                    print(_query);
                                    page = 0;
                                    currentPage = 0;
                                    profiles = List();
                                    loadData();
                                  });
                                },
                                textColor: Colors.white,
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate('search'),
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontFamily: 'Hacen',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                color: repo.blue3,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              profiles.length > 0
                  ? Column(
                      children: profiles.map((profile) {
                        print(profile.name);
                        return createTileProfile(
                            profile,
                            getColorProfile(profile.specialty.parentId),
                            getLeftRightImg(profile.specialty.parentId));
                      }).toList(),
                    )
                  : isLoading
                      ? Container()
                      : Center(
                          child: Text(AppLocalizations.of(context)
                              .translate('no_results_found')),
                        ),

              isLoading
                  ? Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('loading...'),
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.black87,
                              ),
                            )),
                      ],
                    )
                  : Container(),

              //getMedicalProfiles('major_id=1');

//          FutureBuilder<Response>(
//              future: getMedicalProfiles(_query), // query
//              builder: (context, snapshot) {
//                if (snapshot.connectionState == ConnectionState.done) {
//                  if (snapshot.hasData) {
//                    if (snapshot.data.statusCode == 200) {
//                      List<MedicalProfile> medicalProfiles =
//                          snapshot.data.object;
//
//                      return medicalProfiles.length > 0
//                          ? Column(
//                              children: medicalProfiles
//                                  .map((profile) => createTileProfile(
//                                      profile,
//                                      getColorProfile(
//                                          profile.specialty.parentId),
//                                      getLeftRightImg(
//                                          profile.specialty.parentId)))
//                                  .toList(),
//                            )
//                          : Center(
//                              child: Text('لا توجد نتائج'),
//                            );
//                    } else
//                      return Center(
//                        child: Text('حدث خطأ ما'),
//                      );
//                  } else
//                    return Center(
//                      child: Text('no data'),
//                    );
//                } else {
//                  return Column(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: CircularProgressIndicator(),
//                      ),
//                      Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: Text(
//                            'جارٍ التحميل .. ',
//                            style: TextStyle(
//                              fontSize: 18.0,
//                              color: Colors.black87,
//                            ),
//                          )),
//                    ],
//                  );
//                }
//              }),
            ],
          ),
        ),
      ],
    );
  }
}
