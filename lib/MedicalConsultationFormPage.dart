
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:http/http.dart' as http;
import 'app_localizations.dart';
class MedicalConsultationFormPage extends StatefulWidget {
  const MedicalConsultationFormPage({Key key}) : super(key: key);

  @override
  _MedicalConsultationFormPageState createState() =>
      _MedicalConsultationFormPageState();
}

class _MedicalConsultationFormPageState
    extends State<MedicalConsultationFormPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void validateAndPost() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      print('validated');
      final url = Uri.parse('https://elaj.care/api/consultation');
      http.Response response = await http.post(url,headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, body:jsonEncode( _formData));
      print(response.statusCode);
      print('////////////////////////////////////////////////////////////');
      if (response.statusCode == 200)
      {
         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.green,
                content: Text(
                  ' ${AppLocalizations.of(context).translate('Successful')}',
                  style: TextStyle(fontFamily: 'Hacen'),
                  textAlign: TextAlign.center,
                ),
              ));
      }
      else
      {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                content: Text(
                  ' ${AppLocalizations.of(context).translate('some_error_occurred')}',
                  style: TextStyle(fontFamily: 'Hacen'),
                  textAlign: TextAlign.center,
                ),
              ));
      }
      print('done');
      print(response.body);
    } else
      print('not validated');
  }
  @override
  void initState() {
    super.initState();
    
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    
  }
  String notEmpty(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return Localizations.localeOf(context).languageCode == 'en' ?'This Field Cannot Be Empty':'هذا الحقل لا يجب أن يكون فارغاً';
  }

  Map<String, String> _formData = {
    'name': null,
    'gender': null,
    'mobile': null,
    'email': null,
    'address': null,
    'description': null,
    'birthday': null,
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
         child: Localizations.localeOf(context).languageCode == 'en' ?Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 50, 0),
          child: Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ):
          Padding(
            padding:EdgeInsets.fromLTRB(50, 0, 0, 0) ,
            child:Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ),

        ),

        backgroundColor: repo.blue1,
       ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Center(
              child: Text(
                Localizations.localeOf(context).languageCode =='ar'?'طلب إستشارة طبية':'Medical Consultation',
                style: TextStyle(
                    color: Color(0xFFFB753F),
                    fontSize: 23.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Form(
              key: formKey,
              child: Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                           onSaved: (value) {
                            setState(() {
                              _formData['name'] = value;
                            });
                          },
                          validator: notEmpty,
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar'? 'الاسم الثلاثي':'Full Name',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: FormBuilderRadioGroup(
                          name: 'Gender',
                         onSaved: (value) {
                            setState(() {
                              _formData['description'] =(value == 'ذكر' || value == 'male') ? 'male' : 'female';
                            });
                                
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar'?'الجنس':'Gender',
                            labelStyle: TextStyle(
                              fontSize: 30.0,
                            ),
                          ),

                          //attribute: "best_language",
                          //leadingInput: true,
                          //onChanged: _onChanged,
                          //validators: [FormBuilderValidators.required()],
                          options: [Localizations.localeOf(context).languageCode =='ar'?'أنثى':'female',  Localizations.localeOf(context).languageCode =='ar'?'ذكر':'male'    ]
                              .map((lang) => FormBuilderFieldOption(
                                    value: lang,
                                    child: Text('$lang'),
                                  ))
                              .toList(growable: false),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onSaved: (value) {
                            setState(() {
                              _formData['email'] = value;
                            });
                          },
                          textDirection: TextDirection.ltr,
                          validator: EmailValidator(
                              errorText: 'البريد الإلكتروني غير صالح'),
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar'? 'البريد الإلكتروني':'email',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                        onSaved: (value) {
                            setState(() {
                              _formData['address'] = value;
                            });
                          },
                          validator: notEmpty,
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar'?'العنوان':'Address',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                         onSaved: (value) {
                            setState(() {
                              _formData['birthday'] = value;
                            });
                          },
                          validator: notEmpty,
                          textDirection: TextDirection.ltr,
                          decoration: InputDecoration(
                            labelText: 'YYYY/MM/DD',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                         onSaved: (value) {
                            setState(() {
                              _formData['mobile'] = value;
                            });
                          },
                          validator: notEmpty,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          textDirection: TextDirection.ltr,
                          decoration: InputDecoration(
                            labelText:Localizations.localeOf(context).languageCode =='ar'? 'رقم الهاتف':'Phone Number',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onSaved: (value) {
                            setState(() {
                              _formData['description'] = value;
                            });
                            
                          },
                          validator: notEmpty,
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar'?'أعراض الحالة المرضية':'Symptoms',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: RaisedButton(
                          onPressed: validateAndPost,
                          color: Colors.blue[300],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                          child: Text(Localizations.localeOf(context).languageCode =='en'?'Submit':'إرسال'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
