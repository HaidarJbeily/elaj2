import 'package:flutter/material.dart';

import 'package:elag_app/PublicSearch.dart';
import 'Search.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'connectionmanager.dart';
import 'package:elag_app/OpenPharmacies.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'app_localizations.dart';

class ProfileDialog extends StatefulWidget {
  int id;
  List<Specialty> spec;
  String title;
  String imgPath;
  Color col;
  String defaultImage;

  ProfileDialog(this.id, this.spec, this.title, this.imgPath, this.col,
      this.defaultImage);

  @override
  _ProfileDialogState createState() => _ProfileDialogState();
}

class _ProfileDialogState extends State<ProfileDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return new AlertDialog(
      title: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            widget.imgPath,
            fit: BoxFit.contain,
            height: 35.0,
            width: 35.0,
          ),
          Padding(padding: EdgeInsets.all(8.0)),
          Text(
            widget.title??'',
            style: new TextStyle(
              color: widget.col,
              fontSize: 16.0,
              fontFamily: 'Hacen',
            ),
          ),
        ],
      )),
      content: widget.spec.length > 0
          ? Padding(
              padding: EdgeInsets.only(top: 22.0),
              child: Container(
                height: width / 1.4,
                width: width,
                child: ListView(
                  children: widget.spec
                      .map(
                        (sp) => Column(
                          children: <Widget>[
                            ListTile(
                              title: Padding(
                                padding:
                                    EdgeInsets.only(top: 16.0, bottom: 0.0),
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: sp.icon == null
                                          ? AssetImage(widget.defaultImage)
                                          : NetworkImage(
                                              '$baseUrl/storage/${sp.icon}'),
                                      width: 35.0,
                                      height: 35.0,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 15.0)),
                                    Expanded(
                                      child: AutoSizeText(
                                        sp.name==null?"Not":sp.name,
                                        maxLines: 1,
                                        minFontSize: 2.0,
                                        textAlign: TextAlign.end,
                                        textDirection:Localizations.localeOf(context).languageCode =='ar'?TextDirection.ltr:TextDirection.rtl,
                                        style: new TextStyle(
                                          color: Colors.black.withOpacity(0.7),
                                          fontSize: 18.0,
                                          fontFamily: 'Hacen',
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        Search(sp, widget.spec,Localizations.localeOf(context).languageCode)));
                              },
                              enabled: true,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: Divider(
                                height: 0.0,
                                color: Colors.black54,
                              ),
                            ),
                          ],
                        ),
                      )
                      .toList(),
                ),
              ),
            )
          : Padding(
              padding: EdgeInsets.only(top: 22.0),
              child: Container(
                height: width / 1.4,
                width: width,
                child: FutureBuilder<Response>(
                    future: getSpecialties(Localizations.localeOf(context).languageCode), // query
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasData) {
                          if (snapshot.data.statusCode == 200) {
                            Specialties = snapshot.data.object;
                            widget.spec = new List();
                            for (int i = 0; i < Specialties.length; i++) {
                              if (Specialties[i].parentId == widget.id) {
                                widget.spec.add(Specialties[i]);
                              }
                            }
                            return widget.spec.length > 0
                                ? ListView(
                                    children: widget.spec
                                        .map(
                                          (sp) => Column(
                                            children: <Widget>[
                                              ListTile(
                                                title: Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 16.0, bottom: 0.0),
                                                  child: Text(
                                                    sp.name ??'',
                                                    style: new TextStyle(
                                                      color: Colors.black
                                                          .withOpacity(0.7),
                                                      fontSize: 18.0,
                                                      fontFamily: 'Hacen',
                                                    ),
                                                  ),
                                                ),
                                                onTap: () {
                                                  Navigator.of(context).pop();
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              Search(
                                                                  sp,
                                                                  widget
                                                                      .spec,Localizations.localeOf(context).languageCode)));
                                                },
                                                enabled: true,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10.0, right: 10.0),
                                                child: Divider(
                                                  height: 0.0,
                                                  color: Colors.black54,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                        .toList(),
                                  )
                                : Container();
                          } else
                            return Center(
                              child: Text(AppLocalizations.of(context)
                                  .translate('some_error_occurred')),
                            );
                        } else
                          return Center(
                            child: Text('no data'),
                          );
                      } else {
                        return Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate('loading...'),
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.black87,
                                  ),
                                )),
                          ],
                        );
                      }
                    }),
              ),
            ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            AppLocalizations.of(context).translate('cancel'),
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.red,
            ),
          ),
        ),
      ],
      contentPadding:
          EdgeInsets.only(top: 0.0, left: 4.0, right: 4.0, bottom: 0.0),
    );
  }
}

class LocationDialog extends StatefulWidget {
  List<Location> Loc;
  String title;
  String imgPath;
  Color col;

  LocationDialog(this.Loc, this.title, this.imgPath, this.col);

  @override
  _LocationDialogState createState() => _LocationDialogState();
}

class _LocationDialogState extends State<LocationDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return new AlertDialog(
      title: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            widget.imgPath,
            fit: BoxFit.contain,
            height: 35.0,
            width: 35.0,
          ),
          Padding(padding: EdgeInsets.all(8.0)),
          Text(
            widget.title ?? '',
            style: new TextStyle(
              color: widget.col,
              fontSize: 24.0,
              fontFamily: 'Hacen',
            ),
          ),
        ],
      )),
      content: Padding(
        padding: EdgeInsets.only(top: 22.0),
        child: Container(
          height: width / 1.3,
          width: width,
          child: ListView(
            children: Countries.map(
              (country) => ExpansionTile(
                title: Text(
                  country.name??'',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 20.0,
                    fontFamily: 'Hacen',
                  ),
                ),
                children: Locations.map(
                  (loc) => loc.parentId == country.id
                      ? ListTile(
                          title: Text(
                            loc.name??'',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.65),
                              fontSize: 18.0,
                              fontFamily: 'Hacen',
                            ),
                          ),
                          enabled: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => PublicSearch(loc, null)));
                          },
                          contentPadding:
                              EdgeInsets.only(left: 25.0, right: 25.0),
                        )
                      : Container(),
                ).toList(),
              ),
            ).toList(),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            AppLocalizations.of(context).translate('cancel'),
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.red,
              fontFamily: 'Regular',
            ),
          ),
        ),
      ],
      contentPadding:
          EdgeInsets.only(top: 0.0, left: 4.0, right: 4.0, bottom: 0.0),
    );
  }
}

class OpenPharmaciesDialog extends StatefulWidget {
  List<Location> Loc;
  String title;
  String imgPath;
  Color col;

  OpenPharmaciesDialog(this.Loc, this.title, this.imgPath, this.col);

  @override
  _OpenPharmaciesDialogState createState() => _OpenPharmaciesDialogState();
}

class _OpenPharmaciesDialogState extends State<OpenPharmaciesDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return new AlertDialog(
      title: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            widget.imgPath,
            fit: BoxFit.contain,
            height: 35.0,
            width: 35.0,
          ),
          Padding(padding: EdgeInsets.all(8.0)),
          Text(
            widget.title??'',
            style: new TextStyle(
              color: widget.col,
              fontSize: 24.0,
              fontFamily: 'Hacen',
            ),
          ),
        ],
      )),
      content: Padding(
        padding: EdgeInsets.only(top: 22.0),
        child: Container(
          height: width / 1.3,
          width: width,
          child: ListView(
            children: Countries.map(
              (country) => ExpansionTile(
                title: Text(
                  country.name??'',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 20.0,
                    fontFamily: 'Hacen',
                  ),
                ),
                children: Locations.map(
                  (loc) => loc.parentId == country.id
                      ? ListTile(
                          title: Text(
                            loc.name??'',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.65),
                              fontSize: 18.0,
                              fontFamily: 'Hacen',
                            ),
                          ),
                          enabled: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => OpenPharmacies(
                                  loc.id.toString(),Localizations.localeOf(context).languageCode
                                ),
                              ),
                            );
                          },
                          contentPadding:
                              EdgeInsets.only(left: 25.0, right: 25.0),
                        )
                      : Container(),
                ).toList(),
              ),
            ).toList(),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            AppLocalizations.of(context).translate('cancel'),
            style: TextStyle(
              fontSize: 18.0,
              color: Colors.red,
              fontFamily: 'Regular',
            ),
          ),
        ),
      ],
      contentPadding:
          EdgeInsets.only(top: 0.0, left: 4.0, right: 4.0, bottom: 0.0),
    );
  }
}
