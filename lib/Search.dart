import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:elag_app/MyDrawer.dart';
import 'Profile.dart';
import 'package:elag_app/Model/MedicalProfile.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/MedicalAdverts.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'connectionmanager.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'app_localizations.dart';

class Search extends StatelessWidget {
  List<Color> _colorProfile;
  Specialty _selectedSpecialty;
  List<Specialty> _specialty;
  String _titleAppBar = '';
  String _titleText = '';
  String CountryCode;
  Search(this._selectedSpecialty, this._specialty,this.CountryCode){
    print(this._selectedSpecialty.name);
    print(12314415);
    print("Search :: CountryCode");
    print(this.CountryCode);
  }

  void getTitle(int id) {
    if (id == 1) {
      _titleAppBar =
         this.CountryCode == 'ar' ? 'الأطباء' : 'Doctors';
      _titleText =
          this.CountryCode == 'ar' ? 'الطبيب' : 'Doctor';
    } else if (id == 2) {
      _titleAppBar =this.CountryCode == 'ar'
          ? 'المراكز الطبية'
          : 'Medical Centers';
      _titleText = this.CountryCode == 'ar'
          ? 'المركز'
          : 'Medical Center';
    } else if (id == 3) {
      _titleAppBar = this.CountryCode == 'ar'
          ? 'الأكاديمية الطبية'
          : 'Medical Academy';
      _titleText = this.CountryCode == 'ar'
          ? 'الأكاديمية'
          : 'Academy';
    } else if (id == 4) {
      _titleAppBar = this.CountryCode == 'ar'
          ? 'الصيدليات'
          : 'Pharmacies';
      _titleText =
          this.CountryCode == 'ar' ? 'الصيدلية' : 'Pharmacy';
    } else if (id == 125) {
      _titleAppBar =this.CountryCode == 'ar'
          ? 'أطباء أسنان'
          : 'Dentists';
      _titleText =
          this.CountryCode == 'ar' ? 'الطبيب' : 'Dentist';
    } else if (id == 97) {
      _titleAppBar =  this.CountryCode== 'ar'
          ? 'خدمات طبية'
          : 'Medical Services';
      _titleText =
          this.CountryCode == 'ar' ? 'الخدمة' : 'Service';
    }
  }

  List<Color> getColorProfile(int id) {
    List<Color> colors = new List();
    if (id == 1) {
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    } else if (id == 2) {
      colors.add(repo.red3);
      colors.add(repo.red2);
      colors.add(repo.red1);
    } else if (id == 3) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else if (id == 4) {
      colors.add(repo.green3);
      colors.add(repo.green2);
      colors.add(repo.green1);
    } else if (id == 125) {
      colors.add(repo.tooth3);
      colors.add(repo.tooth2);
      colors.add(repo.tooth1);
    } else if (id == 97) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else {
      // Exception
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    }
    return colors;
  }

  @override
  Widget build(BuildContext context) {
    _colorProfile = getColorProfile(_selectedSpecialty.parentId);
    getTitle(_selectedSpecialty.parentId);
    print(_titleAppBar);
    print(_selectedSpecialty);
    print(_titleText);
    print(_specialty);
    
    print(1212131243254213);
    return HomeSearch(_selectedSpecialty, _titleAppBar, _titleText,
        _colorProfile, _specialty);
  }
}

class HomeSearch extends StatefulWidget {
  List<Color> _colorProfile;
  List<Specialty> _specialty;
  Specialty _selectedSpecialty;
  String _titleAppBar;
  String _titleText;

  HomeSearch(this._selectedSpecialty, this._titleAppBar, this._titleText,
      this._colorProfile, this._specialty);

  @override
  _HomeSearchState createState() => _HomeSearchState();
}

class _HomeSearchState extends State<HomeSearch> {
  TextEditingController _controllerSearch;
  int _selectedSpecialityId;
  Location _valDropCity;
  String _imgLeft;
  String _imgRight;
  String _querySpecialty = '';
  String _queryLocation = '';
  String _queryName = '';
  String _query = '';
  int page, currentPage;
  List<Location> sortLocations;
  List<MedicalProfile> profiles;
  bool isLoading;
  ScrollController _scrollController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void buildQuery() {
    _query = '';
    if (_queryLocation != '') {
      _query = '${_query}${_queryLocation}&';
    }
    if (_querySpecialty != '') {
      _query = '${_query}${_querySpecialty}&';
    }
    if (_queryName != '') {
      _query = '$_query${_queryName}&';
    }
    print(_query);
  }

  void getLeftRightImg() {
    if (widget._selectedSpecialty.parentId == 1) {
      _imgLeft = 'images/doctorSearchLeft.png';
      _imgRight = 'images/doctorSearchRight.png';
    } else if (widget._selectedSpecialty.parentId == 2) {
      _imgLeft = 'images/centerSearchLeft.png';
      _imgRight = 'images/centerSearchRight.png';
    } else if (widget._selectedSpecialty.parentId == 3) {
      _imgLeft = 'images/academicSearchLeft.png';
      _imgRight = 'images/academicSearchRight.png';
    } else if (widget._selectedSpecialty.parentId == 4) {
      _imgLeft = 'images/pharamSearchLeft.png';
      _imgRight = 'images/pharamSearchRight.png';
    } else if (widget._selectedSpecialty.parentId == 125) {
      _imgLeft = 'images/toothSearchLeft.png';
      _imgRight = 'images/toothSearchRight.png';
    } else if (widget._selectedSpecialty.parentId == 97) {
      _imgLeft = 'images/servicesMedicalSearchLeft.png';
      _imgRight = 'images/servicesMedicalSearchRight.png';
    } else {
      // Exception
      _imgLeft = 'images/doctorSearchLeft.png';
      _imgRight = 'images/doctorSearchRight.png';
    }
  }
   @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    page = 0;
    currentPage = 0;
    _selectedSpecialityId = widget._selectedSpecialty.id;
  
    sortLocations = new List();
    String LocationTitle = Localizations.localeOf(context).languageCode == "en"? "Choose City ...":"اختر مدينة ....";
    sortLocations.add(new Location(-1,LocationTitle , '-1', '-1', -1, -1));
    _valDropCity = sortLocations[0];
    _controllerSearch = new TextEditingController();
    getLeftRightImg();

    _querySpecialty =
        'specialty=${widget._selectedSpecialty.id},${widget._selectedSpecialty.parentId}';
    _query = _querySpecialty;

    
    profiles = List();
    isLoading = false;
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.extentAfter < 25 &&
          profiles.length % 10 == 0 &&
          !isLoading) {
        loadData();
      }
    });
    loadData();
    if (Countries != null) {
      for (int i = 0; i < Countries.length; i++) {
        sortLocations.add(Countries[i]);
        for (int j = 0; j < Locations.length; j++) {
          if (Locations[j].parentId == Countries[i].id) {
            sortLocations.add(Locations[j]);
          }
        }
      }
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
  }

  void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });

      print('$page , $currentPage');
      page++;
      print('${_query}&page=${page}');
      getMedicalProfiles('${_query}&page=${page}',Localizations.localeOf(context).languageCode).then((response) {
        if (response.statusCode == 200) {
          profiles.addAll(response.object);
          currentPage++;
        }
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;

    List<BoxShadow> listShadow = new List();
    listShadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.1,
    ));
    var searchTextStyle = new TextStyle(
      color: Colors.black54,
      fontSize: 12.0,
      fontFamily: 'Hacen',
    );

    createTileProfile(MedicalProfile profile) {
      print('######');
      print(profile.phone);
//      if (profile != null)
//        return Container(
//          width: 200,
//          height: 20,
//          color: Colors.blue,
//          child: Text(profile.name),
//        );
//      else
      return GestureDetector(
        onTap: () async {
          if (profile.level == 'one') {
            _scaffoldKey.currentState.showSnackBar(SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                '${profile.name} ${AppLocalizations.of(context).translate('is_not_subscribed_to_the_service')}',
                style: TextStyle(fontFamily: 'Hacen'),
                textAlign: TextAlign.center,
              ),
            ));
          } else if (profile.level == 'two') {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Profile(profile)));
          } else if (profile.level == 'three') {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Profile(profile)));
          } else {
            Scaffold.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                AppLocalizations.of(context).translate('some_error_occurred'),
                style: TextStyle(fontFamily: 'Hacen'),
                textAlign: TextAlign.center,
              ),
            ));
          }
        },
        child: Padding(
          padding:
              EdgeInsets.only(left: 15.0, right: 15.0, bottom: 6.0, top: 6.0),
          child: Container(
            height: 110.0,
            decoration: BoxDecoration(
              boxShadow: listShadow,
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(
                      color: widget._colorProfile[1],
                      style: BorderStyle.solid,
                      width: 6.0)),
            ),
            child: Stack(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: CircleAvatar(
                        radius: 40.0, //width / 8.8
                        backgroundColor: widget._colorProfile[1],
                        child: CircleAvatar(
                          radius: 38.5, //width / 9.1
                          backgroundColor: Colors.white,
                          backgroundImage: profile.logo == null
                              ? AssetImage(
                                  _imgRight,
                                )
                              : NetworkImage(
                                  '$baseUrl/storage/${profile.logo}'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 8.0, top: 14.0, bottom: 6.0, right: 8.0),
                      child: Container(
                        width: width / 4.2,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                              image: AssetImage(_imgLeft),
                              fit: BoxFit.contain,
                            )),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  child: AutoSizeText(
                    profile.name != null ? profile.name.trim() : 'Not translated',
                    maxLines: 1,
                    minFontSize: 12.0,
                    maxFontSize: 18.0,
                    textAlign: TextAlign.end,
                    textDirection: Localizations.localeOf(context).languageCode == 'en' ?  TextDirection.rtl:TextDirection.ltr,
                    style: TextStyle(
//                        fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Hacen',
                      color: widget._colorProfile[2],
                    ),
                    //textDirection: TextDirection.rtl,
                  ),
                  top: 10.0,
                  right:Localizations.localeOf(context).languageCode =='ar'?100.0:null,
                  left:Localizations.localeOf(context).languageCode =='ar'?null:100.0, //width / 3.6
                ),
                 Positioned(
                 
                  child: Row(
                    
                    children: <Widget>[
                      Expanded(
//                          height: 55.0,
//                          width: width / 3.2,
                        child: Align(
                          alignment: Alignment.center,
                          child: Column(
                          
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                
                                child: Align(
                                  alignment: Alignment.center,
                                  child: profile.specialty != null
                                      ? Row(
                                          children: <Widget>[
                                            SvgPicture.asset(
                                              'images/specialistProfile.svg',
                                              width: 20.0,
                                              height: 20.0,
                                              color: widget._colorProfile[2],
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  right: 2.0, left: 2.0),
                                            ),
                                            Expanded(
                                              child: profile.specialty.name !=
                                                      null
                                                  ? AutoSizeText(
                                                      profile.specialty.name
                                                          .trim(),
                                                      maxLines: 2,
                                                      minFontSize: 4.0,
                                                      textAlign:
                                                          TextAlign.start,
                                                      textDirection:
                                                          Localizations.localeOf(context).languageCode == 'ar' ?  TextDirection.rtl:TextDirection.ltr,
                                                      style: searchTextStyle,
                                                    )
                                                  : Container(),
                                            ),
                                          ],
                                        )
                                      : Container(),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: profile.location != null
                                      ? Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            Image.asset(
                                              'images/cityPopUp.png',
                                              width: 20.0,
                                              height: 20.0,
                                              color: widget._colorProfile[2],
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  right: 2.0, left: 2.0),
                                            ),
                                            Expanded(
                                              child: profile.location.name !=
                                                      null
                                                  ? AutoSizeText(
                                                      profile.location.name
                                                          .trim(),
                                                      maxLines: 2,
                                                      minFontSize: 4.0,
                                                      textAlign:
                                                          TextAlign.start,
                                                      textDirection:
                                                          Localizations.localeOf(context).languageCode == 'ar' ?  TextDirection.rtl:TextDirection.ltr,
                                                      style: searchTextStyle,
                                                    )
                                                  : Container(),
                                            ),
                                          ],
                                        )
                                      : Container(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 1.0, right: 1.0),
                      ),
                      Expanded(
//                          height: 55.0,
//                          width: width / 3.2,
                        child: Align(
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Row(
                                      children: <Widget>[
                                        Image.asset(
                                          'images/mobile.png',
                                          width: 20.0,
                                          height: 20.0,
                                          color: widget._colorProfile[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 2.0, left: 2.0),
                                        ),
                                        Expanded(
                                          child: profile.mobile != null
                                              ? AutoSizeText(
                                                  profile.mobile.trim(),
                                                  maxLines: 2,
                                                  minFontSize: 4.0,
                                                  textAlign: TextAlign.start,
                                                  textDirection:
                                                      Localizations.localeOf(context).languageCode == 'ar' ?  TextDirection.rtl:TextDirection.ltr,
                                                  style: searchTextStyle,
                                                )
                                              : Container(
                                                  child: Text('-'),
                                                ),
                                        ),
                                      ],
                                    )),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Row(
                                    children: <Widget>[
                                      SvgPicture.asset(
                                        'images/teleProfiles.svg',
                                        width: 20.0,
                                        height: 20.0,
                                        color: widget._colorProfile[2],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right: 2.0, left: 2.0),
                                      ),
                                      Expanded(
                                        child: profile.phone != null
                                            ? AutoSizeText(
                                                profile.phone.trim(),
                                                maxLines: 2,
                                                minFontSize: 4.0,
                                                textAlign: TextAlign.start,
                                                textDirection:
                                                Localizations.localeOf(context).languageCode == 'ar' ?  TextDirection.rtl:TextDirection.ltr,
                                                style: searchTextStyle,
                                              )
                                            : Container(
                                                child: Text('-'),
                                              ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                   top: 45.0,
                  right: Localizations.localeOf(context).languageCode == 'ar'? 100.0:4.0,
                  left:  Localizations.localeOf(context).languageCode == 'ar'? 4.0:100.0,
                  bottom: 4.0,
                   ),
                 
//                    width: width / 1.6,
               
            
                
              ],
            ),
          ),
        ),
      );
    }

    print('build called ${_selectedSpecialityId}');

    return Theme(
      data: new ThemeData(
        splashColor: widget._colorProfile[0],
        primaryColor: widget._colorProfile[1],
        accentColor: widget._colorProfile[1],
        inputDecorationTheme: new InputDecorationTheme(
          labelStyle: new TextStyle(
            fontSize: 15.0,
            fontFamily: 'Hacen',
          ),
        ),
      ),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[200],
        drawer: new Opacity(
          opacity: 0.92,
          child: MyDrawer(CountryCode: Localizations.localeOf(context).languageCode,check: 0,),
        ),
        body: Stack(
          children: <Widget>[

          
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backgroundHomePage.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: width,
              height: 82.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                gradient: LinearGradient(
                    colors: widget._colorProfile, tileMode: TileMode.mirror),
              ),
              child:  Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Text(
                        widget._titleAppBar,
                        style: TextStyle(
                          fontSize: 26.5,
                          //fontWeight: FontWeight.w900,
                          fontFamily: 'Hacen',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
            
              )  ,
              ),
             Positioned(
              
              
              top: Localizations.localeOf(context).languageCode =='ar'?27.0:27.0,
              left: Localizations.localeOf(context).languageCode =='ar'?5.0:null,
              right: Localizations.localeOf(context).languageCode =='ar'?null:5.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: Colors.white,
                        size: 26.0,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ),
              ),
            ),
            Positioned(
             top: Localizations.localeOf(context).languageCode =='en'?42.0:42.0,
              left: Localizations.localeOf(context).languageCode =='en'?16.0:null,
              right: Localizations.localeOf(context).languageCode =='en'?null:18.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: Image.asset(
                  'images/drawerIcon.png',
                  height: 18.0,
                  width: 18.0,
                ),
              ),
            ),
           
            Padding(
              padding: EdgeInsets.only(top: 88.0),
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: width,
                      height: 148.0,
                      decoration: BoxDecoration(
                        boxShadow: listShadow,
                        color: Colors.white,
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            height: 148.0,
                            width: width * 0.58,
                            color: Colors.white,
                            child: Container(
                              padding:
                                  const EdgeInsets.only(left: 2.0, right: 2.0),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(5.0),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: 12.0, left: 12.0),
                                    child: new DropdownButton(
                                        isExpanded: true,
                                        style: TextStyle(
                                          fontSize: 15.0,
                                          fontFamily: 'Hacen',
                                          color: Colors.black,
                                        ),
                                        value: widget._selectedSpecialty,
//                                        icon: Image(
//                                          image: NetworkImage(
//                                              '$baseUrl/storage/${widget._selectedSpecialty.icon}'),
//                                          width: 35.0,
//                                          height: 35.0,
//                                        ),
                                        items: widget._specialty
                                            .map((spec) => DropdownMenuItem(
                                                  value: spec,
                                                  child: AutoSizeText(spec.name==null?"Not Translated":spec.name),
                                                ))
                                            .toList(),
                                        onChanged: (val) {
                                          setState(() {
                                            widget._selectedSpecialty = val;
                                            _selectedSpecialityId =
                                                widget._selectedSpecialty.id;
                                            _querySpecialty =
                                                'specialty=${widget._selectedSpecialty.id},${widget._selectedSpecialty.parentId}';
////                                            _query = _querySpecialty +
//                                                '&' +
//                                                _queryLocation;
                                          });
                                        }),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      right: 12.0,
                                      left: 12.0,
                                    ),
                                    child: new TextFormField(
                                      decoration: new InputDecoration(
                                        labelText:
                                            '${AppLocalizations.of(context).translate('name_of')} ${widget._titleText} .. ',
                                      ),
                                      keyboardType: TextInputType.text,
                                      controller: _controllerSearch,
                                      //onEditingComplete: () {
                                      //},
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: 148.0,
                            width: width * 0.42,
                            color: Colors.white,
                            child: Container(
                              padding: const EdgeInsets.only(
                                  left: 2.0, right: 2.0, bottom: 5.0),
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(5.0),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: 4.0, bottom: 0.0),
                                    child: Center(
                                      child: new DropdownButton(
                                          isExpanded: true,
                                          style: TextStyle(
                                            fontSize: 15.5,
                                            fontFamily: 'Hacen',
                                            color: Colors.black,
                                          ),
                                          value: _valDropCity,
                                          items: sortLocations
                                              .map((loc) => DropdownMenuItem(
                                                    value: loc,
                                                    child: Text(loc.name),
                                                  ))
                                              .toList(),
                                          onChanged: (val) {
                                            setState(() {
                                              _valDropCity = val;
                                              if (_valDropCity.id != -1) {
                                                if (_valDropCity.parentId !=
                                                    null) {
                                                  _queryLocation =
                                                      'location=${_valDropCity.id},${_valDropCity.parentId}';
//                                                  _query = _querySpecialty +
//                                                      '&' +
//                                                      _queryLocation;
                                                } else {
                                                  _queryLocation =
                                                      'location=${_valDropCity.id}';
//                                                  _query = _querySpecialty +
//                                                      '&' +
//                                                      _queryLocation;
                                                }
                                              } else {
//                                                _query = _querySpecialty;
                                              }
                                            });
                                          }),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(13.0),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: 15.0, left: 15.0, bottom: 6.0),
                                    child: new MaterialButton(
                                      height: 35.0,
                                      minWidth: 140.0,
                                      onPressed: () async {
                                        page = 1;
                                        if (_controllerSearch.text != null &&
                                            _controllerSearch.text.length > 0) {
                                          _queryName =
                                              'query=${_controllerSearch.text}';
                                        } else {
                                          _queryName = '';
                                        }
                                        buildQuery();
                                        profiles = List();
                                        getMedicalProfiles(
                                                '${_query}page=${page}',Localizations.localeOf(context).languageCode)
                                            .then((response) {
                                          if (response.statusCode == 200) {
                                            setState(() {
                                              profiles = response.object;
                                            });
                                          }
                                        });
                                      },
                                      textColor: Colors.white,
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate('search'),
                                        style: TextStyle(
                                          fontSize: 18.0,
                                          fontFamily: 'Hacen',
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      color: widget._colorProfile[2],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //////////////////////////////////////////////////////////

                    MedicalAdvertsSearch(_selectedSpecialityId.toString()),
                    //////////////////////////////////////////////////////////////////////////
                    profiles.length > 0
                        ? Column(
                            children: profiles
                                .map((profile) => createTileProfile(profile))
                                .toList(),
                          )
                        : isLoading
                            ? Container()
                            : Center(
                                child: Text(AppLocalizations.of(context)
                                    .translate('no_results_found')),
                              ),

                    isLoading
                        ? Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              ),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate('loading...'),
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.black87,
                                    ),
                                  )),
                            ],
                          )
                        : Container(),
//                    FutureBuilder<Response>(
//                        future: getMedicalProfiles(_query), // query
//                        builder: (context, snapshot) {
//                          if (snapshot.connectionState ==
//                              ConnectionState.done) {
//                            if (snapshot.hasData) {
//                              if (snapshot.data.statusCode == 200) {
//                                List<MedicalProfile> medicalProfiles =
//                                    snapshot.data.object;
//
//                                return medicalProfiles.length > 0
//                                    ? Column(
//                                        children: medicalProfiles
//                                            .map((profile) =>
//                                                createTileProfile(profile))
//                                            .toList(),
//                                      )
//                                    : Center(
//                                        child: Text('لا توجد نتائج'),
//                                      );
//                              } else
//                                return Center(
//                                  child: Text('حدث خطأ ما'),
//                                );
//                            } else
//                              return Center(
//                                child: Text('no data'),
//                              );
//                          } else {
//                            return Column(
//                              mainAxisSize: MainAxisSize.max,
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                Padding(
//                                  padding: const EdgeInsets.all(8.0),
//                                  child: CircularProgressIndicator(),
//                                ),
//                                Padding(
//                                    padding: const EdgeInsets.all(8.0),
//                                    child: Text(
//                                      'جارٍ التحميل .. ',
//                                      style: TextStyle(
//                                        fontSize: 18.0,
//                                        color: Colors.black87,
//                                      ),
//                                    )),
//                              ],
//                            );
//                          }
//                        }),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
