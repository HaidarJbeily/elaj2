class GeoLocation {
  String _lat, _lng;

  GeoLocation(this._lat, this._lng);

  GeoLocation.formJson(Map<String, dynamic> json)
      : this._lat = json['lat'],
        this._lng = json['lng'];

  String get lng => _lng;

  set lng(String value) {
    _lng = value;
  }

  String get lat => _lat;

  set lat(String value) {
    _lat = value;
  }


}
