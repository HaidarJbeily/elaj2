import 'package:elag_app/Model/MedicalProfile.dart';

class PharmacyShift {
  int _id;
  String _day;
  MedicalProfile _profile;

  PharmacyShift(this._id, this._day, this._profile);

  PharmacyShift.fromJson(Map<String, dynamic> json)
      : this._id = json.containsKey('id') ? json['id'] : null,
        this._day = json.containsKey('day') ? json['day'] : null,
        this._profile = json.containsKey('profile')
            ? MedicalProfile.fromJson(json['profile'])
            : null;

  MedicalProfile get profile => _profile;

  set profile(MedicalProfile value) {
    _profile = value;
  }

  String get day => _day;

  set day(String value) {
    _day = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

}
