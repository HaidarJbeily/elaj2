class Location{
  String  _name, _slug, _path;
  int _id, _parentId, _rank;
  Location(this._id,
      this._name,
      this._slug,
      this._path,
      this._rank,
      this._parentId);

  Location.fromJson(Map<String ,dynamic> json):
    _id = json.containsKey('id') ? json['id'] : null,
    _name = json.containsKey('name') ? json['name'] : null,
    _slug = json.containsKey('slug') ? json['slug'] : null,
    _path = json.containsKey('path') ? json['path'] : null,
    _rank = json.containsKey('rank') ? json['rank'] : null,
    _parentId = json.containsKey('parent_id') ? json['parent_id'] : null;


  int get id => this._id;
  set id(int value){
    this._id = value;
  }

  String get name => this._name;
  set name(String value){
    this._name = value;
  }

  String get slug => this._slug;
  set slug(String value){
    this._slug = value;
  }

  String get path => this._path;
  set path(String value){
    this._path = value;
  }

  int get rank => this._rank;
  set rank(int value){
    this._rank = value;
  }


  int get parentId => this._parentId;
  set parentId(int value){
    this._parentId = value;
  }

}