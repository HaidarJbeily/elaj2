import 'package:elag_app/Model/MedicalProfile.dart';

class Research {
  int _id;
  String _title, _url, _publishedAt;
  MedicalProfile _author, _publisher;

  Research(this._id, this._title, this._url, this._publishedAt, this._author,
      this._publisher);

  Research.fromJson(Map<String, dynamic> json)
      : _id = json.containsKey('id') ? json['id'] : null,
        _title = json.containsKey('title') ? json['title'] : null,
        _url = json.containsKey('url') ? json['url'] : null,
        _publishedAt =
            json.containsKey('published_at') ? json['published_at'] : null,
        _author = json.containsKey('author')
            ? json['author'] != null
                ? MedicalProfile.fromJson(json['author'])
                : null
            : null,
        _publisher = json.containsKey('publisher')
            ? json['publisher'] != null
                ? MedicalProfile.fromJson(json['publisher'])
                : null
            : null;

  int get id => this._id;

  set id(int val) {
    this._id = val;
  }

  String get title => this._title;

  set title(String val) {
    this._title = val;
  }

  String get url => this._url;

  set url(String val) {
    this._url = val;
  }

  String get publishedAt => this._publishedAt;

  set publishedAt(String val) {
    this._publishedAt = val;
  }

  MedicalProfile get author => this._author;

  set author(MedicalProfile val) {
    this._author = val;
  }

  MedicalProfile get publisher => this._publisher;

  set publisher(MedicalProfile val) {
    this._publisher = val;
  }
}
