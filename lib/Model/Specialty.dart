class Specialty{
  String  _name,_icon;
  int _id, _parentId;

  Specialty(this._id,
      this._name,
      this._parentId);

  Specialty.fromJson(Map<String ,dynamic> json):
    _id = json.containsKey('id') ? json['id'] : null,
    _name = json.containsKey('name') ? json['name'] : null,
    _parentId = json.containsKey('parent_id') ? json['parent_id'] : null,
  _icon = json.containsKey('icon') ? json['icon'] : null;

  int get id => this._id;
  set id(int value){
    this._id = value;
  }

  String get name => this._name;
  set name(String value) {
    this._name = value;
  }

  int get parentId => this._parentId;
  set parentId(int value){
    this._parentId = value;
  }

  String get icon => _icon;

  set icon(String value) {
    _icon = value;
  }


}