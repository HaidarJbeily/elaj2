import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/GeoLocation.dart';

class MedicalProfile {
  String _name,
      _slug,
      _address,
      _description,
      _logo,
      _email,
      _website,
      _map,
      _mobile,
      _phone,
      _level,
      _facebook,
      _book_appointment,
      _youtube,
      _instagram,
      _telegram;
  Specialty _specialty;
  Location _location;
  int _id, _views;
  GeoLocation _geoLocation;

  MedicalProfile(
      this._id,
      this._name,
      this._slug,
      this._views,
      this._address,
      this._description,
      this._logo,
      this._email,
      this._website,
      this._map,
      this._mobile,
      this._phone,
      this._facebook,
      this._specialty,
      this._location,
      this._level,
      this._geoLocation,
      this._instagram,
      this._youtube,
      this._book_appointment,
      this._telegram);

  MedicalProfile.fromJson(Map<String, dynamic> json)
      : _id = json.containsKey('id') ? json['id'] : null,
        _level = json.containsKey('level') ? json['level'] : null,
        _name = json.containsKey('name') ? json['name'] : null,
        _slug = json.containsKey('slug') ? json['slug'] : null,
        _views = json.containsKey('views') ? json['views'] : null,
        _address = json.containsKey('address') ? json['address'] : null,
        _description =
            json.containsKey('description') ? json['description'] : null,
        _logo = json.containsKey('logo') ? json['logo'] : null,
        _email = json.containsKey('email') ? json['email'] : null,
        _website = json.containsKey('website') ? json['website'] : null,
        _map = json.containsKey('map') ? json['map'] : null,
        _mobile = json.containsKey('mobile') ? json['mobile'] : null,
        _phone = json.containsKey('phone') ? json['phone'] : null,
        _facebook = json.containsKey('facebook') ? json['facebook'] : null,
        _youtube = json.containsKey('youtube') ? json['youtube'] : null,
        _instagram = json.containsKey('instagram') ? json['instagram'] : null,
        _telegram = json.containsKey('telegram') ? json['telegram'] : null,
        _book_appointment = json.containsKey('book_appointment')
            ? json['book_appointment']
            : null,
        _specialty = json.containsKey('specialty')
            ? json['specialty'] != null
                ? Specialty.fromJson(json['specialty'])
                : null
            : null,
        _location = json.containsKey('location')
            ? json['location'] != null
                ? Location.fromJson(json['location'])
                : null
            : null,
        this._geoLocation = json.containsKey('geolocation')
            ? json['geolocation'] != null
                ? json['geolocation'][0] != null
                    ? GeoLocation.formJson(json['geolocation'][0])
                    : null
                : null
            : null;

  int get id => this._id;

  set id(int value) {
    this._id = value;
  }

  get level => _level;

  set level(value) {
    _level = value;
  }

  String get name => this._name;

  set name(String value) {
    this._name = value;
  }

  String get slug => this._slug;

  set slug(String value) {
    this._slug = value;
  }

  int get views => this._views;

  set views(int val) {
    this._views = val;
  }

  String get address => this._address;

  set address(String val) {
    this._address = val;
  }

  String get description => this._description;

  set description(String val) {
    this._description = val;
  }

  String get logo => this._logo;

  set logo(String val) {
    this._logo = val;
  }

  String get email => this._email;

  set email(String val) {
    this._email = val;
  }

  String get website => this._website;

  set website(String val) {
    this._website = val;
  }

  String get map => this._map;

  set map(String val) {
    this._map = val;
  }

  String get mobile => this._mobile;

  set mobile(String val) {
    this._mobile = val;
  }

  String get phone => this._phone;

  set phone(String val) {
    this._phone = val;
  }

  String get facebook => this._facebook;

  set facebook(String val) {
    this._facebook = val;
  }

  Specialty get specialty => this._specialty;

  set specialty(Specialty val) {
    this._specialty = val;
  }

  Location get location => this._location;

  set location(Location val) {
    this._location = val;
  }

  GeoLocation get geoLocation => _geoLocation;

  set geoLocation(GeoLocation value) {
    _geoLocation = value;
  }

  get instagram => _instagram;

  set instagram(value) {
    _instagram = value;
  }

  get telegram => _telegram;

  set telegram(value) {
    _telegram = value;
  }

  get youtube => _youtube;

  set youtube(value) {
    _youtube = value;
  }

  get book_appointment => _book_appointment;

  set book_appointment(value) {
    _book_appointment = value;
  }
}
