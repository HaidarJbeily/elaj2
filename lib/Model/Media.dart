class Media{
  int _id;
  String _path, _type;

  Media(this._id, this._path, this._type);



  Media.fromJson(Map<String ,dynamic> json):
        _id = json.containsKey('id') ? json['id'] : null,
        _path = json.containsKey('path') ? json['path'] : null,
        _type = json.containsKey('type') ? json['type'] : null;

  int get id => this._id;
  set id(int val){
    this._id = val;
  }

  String get path => this._path;
  set path(String val){
    this._path= val;
  }

  String get type => this._type;
  set type(String val){
    this._type = val;
  }

}