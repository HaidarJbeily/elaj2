class Advert {
  int _id;
  String _title, _body, _image, _target, _expiredAt;
  String _name, _url;

  Advert(this._id, this._title, this._body, this._image, this._target,
      this._expiredAt);

  Advert.fromJson(Map<String, dynamic> json)
      : _id = json.containsKey('id') ? json['id'] : null,
        _title = json.containsKey('title') ? json['title'] : null,
        _body = json.containsKey('body') ? json['body'] : null,
        _image = json.containsKey('image') ? json['image'] : null,
        _target = json.containsKey('target') ? json['target'] : null,
        _expiredAt = json.containsKey('expiredAt') ? json['expiredAt'] : null,
        _url = json.containsKey('url') ? json['url'] : null;

  int get id => this._id;

  set id(int val) {
    this._id = val;
  }

  String get title => this._title;

  set title(String val) {
    this._title = val;
  }

  String get body => this._body;

  set body(String val) {
    this._body = val;
  }

  String get image => this._image;

  set image(String val) {
    this._image = val;
  }

  String get target => this._target;

  set target(String val) {
    this._target = val;
  }

  String get expiredAt => this._expiredAt;

  set expiredAt(String val) {
    this._expiredAt = val;
  }

  String get namee => this._name;

  set name(String val) {
    this._name = val;
  }

  String get url => this._url;

  set url(String val) {
    this._url = val;
  }
}
