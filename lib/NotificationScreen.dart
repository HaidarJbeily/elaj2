
import 'package:elag_app/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'Model/notification.dart';
import 'connectionmanager.dart';
import 'package:url_launcher/url_launcher.dart';



class NotifictionScreen extends StatefulWidget {
  

  @override
  _NotifictionScreenState createState() => _NotifictionScreenState();
}

class _NotifictionScreenState extends State<NotifictionScreen> {

  int page;
  int currentPage;
  List<NotificationDetail> notifications;

  bool isLoading;
  ScrollController _scrollController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() { 
    super.initState();
    
  }

   void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });
      print('$page , $currentPage');
      page++;
      getNotifications('page=${page}',Localizations.localeOf(context).languageCode).then((response) {
        if (response.statusCode == 200) {
          notifications.addAll(response.object);
          currentPage++;
        }

        setState(
          () {
            isLoading = false;
           }
        );
      });
    }
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    notifications = [];
    page = 0;
    currentPage = 0;
    isLoading = false;
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.extentAfter < 25 &&
          notifications.length % 10 == 0 &&
          !isLoading &&
          page == currentPage) {
        loadData();
      }
    });
    loadData();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    createTile(NotificationDetail res) => new Card(
                  child: ListTile(title: Text(res.title??''),
                  subtitle: Text(res.body??''),
                  leading:
                  (res.image != null)?
                    ImageIcon(
                         NetworkImage('$baseUrl/storage/${res.image}')
                    ): Icon(Icons.notifications_active_rounded) ,
                
                  onTap: () async {
                              String url = res.url;
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                ),
              );

               



    return Theme(
      data: new ThemeData(
        splashColor: repo.blue3,
        primaryColor: repo.blue2,
        accentColor: repo.blue2,
        inputDecorationTheme: new InputDecorationTheme(
          labelStyle: new TextStyle(
            fontSize: 15.0,
            fontFamily: 'Hacen',
          ),
        ),
      ),
      child: Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Center(
         child: Localizations.localeOf(context).languageCode == 'en' ?Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 50, 0),
          child: Text('Notifications')
          ):
          Padding(
            padding:EdgeInsets.fromLTRB(50, 0, 0, 0) ,
            child:Text('الإشعارات')
          ),

        ),

        backgroundColor: repo.blue1,
       ),
       body:Padding(
      padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
      child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: <Widget>[
              
              notifications.length > 0
                  ? Column(
                      children:
                          notifications.map((res) => createTile(res)).toList())
                  : isLoading
                      ? Container()
                      : Center(
                          child: Text(AppLocalizations.of(context)
                              .translate('no_results_found')),
                        ),
              isLoading
                  ? Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('loading...'),
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.black87,
                              ),
                            )),
                      ],
                    )
                  : Container(),
            ],
          )),
        ),
       ),
       );
  }
  
}