import 'package:flutter/material.dart';




Color blue1 = new Color(0xFF1C5BAD);
Color blue2 = new Color(0xFF005ED8);
Color blue3 = new Color(0xFF0090FF);

Color red1 = new Color(0xFFC8262B);
Color red2 = new Color(0xFFF02A3D);
Color red3 = new Color(0xFFFF7F83);

Color green1 = new Color(0xFF05A854);
Color green2 = new Color(0xFF00C040);
Color green3 = new Color(0xFF55FF8A);

Color purple2 = new Color(0xFF720261);
Color purple3 = new Color(0xFFFF55B3);
Color purple1 = new Color(0xFF6A0047);



Color yellow1 = new Color(0xFFFF742C);
Color yellow2 = new Color(0xFFFF922C);
Color yellow3 = new Color(0xFFFFDB7A);


Color black1 = new Color(0xFF050505);
Color black2 = new Color(0xFF333333);
Color black3 = new Color(0xFF575757);

Color tooth1 = new Color(0xFF008E7E);
Color tooth2 = new Color(0xFF139889);
Color tooth3 = new Color(0xFF0DBD98);