import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:elag_app/Model/Advert.dart';

import 'connectionmanager.dart';
import 'package:url_launcher/url_launcher.dart';

class MedicalAdverts extends StatefulWidget {
  @override
  _MedicalAdvertsState createState() => _MedicalAdvertsState();
}

class _MedicalAdvertsState extends State<MedicalAdverts>
    with SingleTickerProviderStateMixin {
  String _current = '';
  List<String> list = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    list.add('advert1.jpg');
    list.add('advert2.jpg');
    list.add('advert3.jpg');
    loadAverts();
  }

  void loadAverts() async {
    var response = await getAdverts('type=home&c=10',Localizations.localeOf(context).languageCode);
    if (response.statusCode == 200) {
      Adverts = response.object;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      padding: EdgeInsets.all(5.0),
      child: Adverts.length > 0
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        aspectRatio: 2.0,
                        autoPlay: true,
                        autoPlayAnimationDuration: Duration(milliseconds: 2500),
                        viewportFraction: 1.0,
                        initialPage: 0,
                        reverse: false,
                        autoPlayInterval: const Duration(
                          seconds: 8,
                        ),
                        autoPlayCurve: Curves.fastOutSlowIn,
                      ),
                      items: Adverts.map((ads) {
                        return GestureDetector(
                          onTap: () async {
                            print('${ads.url}  ${ads.id}');
                            if (await canLaunch(ads.url)) {
                              launch(ads.url);
                            } else {
                              print('cannot open');
                            }
                          },
                          child: Container(
                            decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(
                                  color: Colors.black54,
                                  width: 0.5,
                                  style: BorderStyle.solid),
                              color: Colors.black38,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                    '$baseUrl/storage/${ads.image}'),
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ],
            )
          : Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: CarouselSlider(
                      items: list.map(
                        (it) {
                          return GestureDetector(
                            onTap: () {},
                            child: Container(
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(
                                    color: Colors.black54,
                                    width: 0.5,
                                    style: BorderStyle.solid),
                                color: Colors.black38,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage('images/' + it),
                                ),
                              ),
                            ),
                          );
                        },
                      ).toList(),
                      options: CarouselOptions(
                        autoPlay: true,
                        autoPlayAnimationDuration: Duration(milliseconds: 2500),
                        viewportFraction: 1.0,
                        initialPage: 0,
                        reverse: false,
                        autoPlayInterval: const Duration(
                          seconds: 8,
                        ),
                        autoPlayCurve: Curves.fastOutSlowIn,
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

class MedicalAdvertsSearch extends StatefulWidget {
  String specialityId;
  MedicalAdvertsSearch(this.specialityId);
  @override
  _MedicalAdvertsSearchState createState() => _MedicalAdvertsSearchState();
}

class _MedicalAdvertsSearchState extends State<MedicalAdvertsSearch>
    with SingleTickerProviderStateMixin {
  String _current = '';
  List<Advert> Adverts = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadAverts();
  }

  void loadAverts() async {
    String query = 'type=search&c=10&specialty=${widget.specialityId}';
    var response = await getAdverts(query,Localizations.localeOf(context).languageCode);
    if (response.statusCode == 200) {
      this.Adverts = response.object;
      print('adverts length ${this.Adverts.length}');
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
        child: this.Adverts.length > 0
            ? Padding(
                padding: EdgeInsets.all(5.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      child: CarouselSlider(
                        options: CarouselOptions(
                          aspectRatio: 2.0,
                          autoPlay: true,
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 2500),
                          viewportFraction: 1.0,
                          initialPage: 0,
                          reverse: false,
                          autoPlayInterval: const Duration(
                            seconds: 8,
                          ),
                          autoPlayCurve: Curves.fastOutSlowIn,
                        ),
                        items: this.Adverts.map((ads) {
                          return GestureDetector(
                            onTap: () async {
                              print('${ads.url}  ${ads.id}');
                              if (await canLaunch(ads.url)) {
                                launch(ads.url);
                              } else {
                                print('cannot open');
                              }
                            },
                            child: Container(
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(
                                    color: Colors.black54,
                                    width: 0.5,
                                    style: BorderStyle.solid),
                                color: Colors.black38,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      '$baseUrl/storage/${ads.image}'),
                                ),
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              )
            : Container());
  }
}
