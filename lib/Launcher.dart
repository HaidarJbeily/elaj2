
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:elag_app/connectionmanager.dart';

class Launcher extends StatefulWidget {
  @override
  _LauncherState createState() => _LauncherState();
}

class _LauncherState extends State<Launcher>
    with SingleTickerProviderStateMixin {
  Timer _timer;
  AnimationController animationController;
  Animation<double> animation;

  void getData() async {
    majorSpecialties = new List();
    Specialties = new List();
    Countries = new List();
    Locations = new List();
    Notifications = [];
    var response1 = await getMajorSpecialties(Localizations.localeOf(context).languageCode);
    if (response1.statusCode == 200) {
      majorSpecialties = response1.object;
    }

    var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
    if (response2.statusCode == 200) {
      Specialties = response2.object;
    }

    var response3 = await getCountries(Localizations.localeOf(context).languageCode);
    if (response3.statusCode == 200) {
      Countries = response3.object;
    }

    var response4 = await getLocations(Localizations.localeOf(context).languageCode);
    if (response4.statusCode == 200) {
      Locations = response4.object;
    }

    var response5 = await getAboutApp(Localizations.localeOf(context).languageCode);
    if (response5.statusCode == 200) {
      aboutApp = response5.object;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(
        duration: Duration(milliseconds: 2000), vsync: this);
    CurvedAnimation curve =
        CurvedAnimation(parent: animationController, curve: Curves.bounceInOut);
    animation = Tween(begin: 0.0, end: 1.0).animate(curve);
    animationController.forward();
    getData();
    Future.delayed(new Duration(seconds: 8), () {
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/HomePage', (Route<dynamic> route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    /*_timer = new Timer(new Duration(seconds: 5), () async {
      Navigator.of(context).pop();
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => HomePage()));
    });*/
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage('images/LauncherBackGround.png'),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(32.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GrowTransition(
                      animation: animation,
                      width: 200.0,
                      height: 200.0,
                      child: Image(
                        image: AssetImage('images/menu-logo.png'),
                        fit: BoxFit.fitWidth,
                      ))
                ],
              ),
              flex: 12,
            ),
            new Expanded(
              flex: 1,
              child: new Container(
                padding: EdgeInsets.fromLTRB(100.0, 0.0, 100.0, 0.0),
                child: new Image.asset('images/Launcher_footer.png'),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}

class GrowTransition extends StatelessWidget {
  GrowTransition(
      {this.child, this.animation, this.width = double.infinity, this.height});

  final Widget child;
  final Animation<double> animation;
  final double width, height;

  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
          animation: animation,
          builder: (BuildContext context, Widget child) {
            return Container(
                height: height * animation.value,
                width: width == double.infinity
                    ? double.infinity
                    : width * animation.value,
                child: child);
          },
          child: child),
    );
  }
}
