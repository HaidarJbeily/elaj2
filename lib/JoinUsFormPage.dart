import 'dart:ffi';

import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'connectionmanager.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class SubscribeFormPage extends StatefulWidget {
  const SubscribeFormPage({Key key}) : super(key: key);

  @override
  _SubscribeFormPageState createState() => _SubscribeFormPageState();
}

class _SubscribeFormPageState extends State<SubscribeFormPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  List<Location> loc;
  void validate() {
    if (formKey.currentState.validate())
      print('validated');
    else
      print('not validated');
  }

  Map<String, dynamic> _formData = {
    'nameAR': null,
    'nameEN': null,
    'specialty': null,
    'city': null,
    'profilePicture': null,
    'addressAR': null,
    'addressEN': null,
    'aboutAR': null,
    'aboutEN': null,
    'phoneNumber': null,
    'mobileNumber': null,
    'email': null,
    'website': null,
    'facebook': null,
    'telegram': null,
  };

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }


  void loadData() async{
    var response4 = await getLocations(Localizations.localeOf(context).languageCode);
    if (response4.statusCode == 200) {
      Locations = response4.object;
    } 
    var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
    if (response2.statusCode == 200) {
      Specialties = response2.object;
    }
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    loadData();
  }


  @override
  Widget build(BuildContext context) {

    createCity(Location city) => DropdownMenuItem<String>(
      child: Text(city.name!=null?'${city.name[0].toUpperCase()}${city.name.substring(1)}':''),
      value: city.name??'',
    );

    createSpeciality(Specialty spec) => DropdownMenuItem<String>(
      child: Text(spec.name!=null?'${spec.name[0].toUpperCase()}${spec.name.substring(1)}':''),
      value: spec.name??'',
    );
    
    return Scaffold(
      appBar: AppBar(
        title: Center(
         child: Localizations.localeOf(context).languageCode == 'en' ?Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 50, 0),
          child: Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ):
          Padding(
            padding:EdgeInsets.fromLTRB(50, 0, 0, 0) ,
            child:Image(
          
            image: ResizeImage(
              AssetImage('images/new_logo.png'),
              width: 60,
              height: 40,
            ),
          ),
          ),

        ),

        backgroundColor: repo.blue1,
       ),
      body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
        
          children: [
            Center(
              child: Text(
                 AppLocalizations.of(context)
                                    .translate('join_us'),
                style: TextStyle(
                    color: Color(0xFFFB753F),
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
                  Form(
                       key: formKey,
                      
                      child: Column(
                    
                          children: [
                      GestureDetector(
                        onTap: () async {
                          var loadedImage = await ImagePicker().pickImage(
                              source: ImageSource.gallery, imageQuality: 50);

                          setState(() {
                            _formData['profilePicture'] =
                                File(loadedImage.path);
                          });
                        },
                        child: CircleAvatar(
                          radius: 55,
                          backgroundColor: Colors.lightBlueAccent,
                          child: _formData['profilePicture'] != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(50),
                                  child: Image.file(
                                    _formData['profilePicture'],
                                    width: 100,
                                    height: 100,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.circular(50)),
                                  width: 100,
                                  height: 100,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['nameAR'] = value;
                          },
                          decoration: InputDecoration(
                            labelText:  AppLocalizations.of(context)
                                    .translate('name'),
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['nameEN'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'الاسم باللغة الإنكليزية':'Name (English)',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      /////////////////////
                      
                       Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                             _formData['specialty'] = value;
                          },
                          decoration: InputDecoration(
                            labelText:  AppLocalizations.of(context)
                                    .translate('specialty') ,
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                              _formData['city'] = value;
                          },
                          decoration: InputDecoration(
                            labelText:  AppLocalizations.of(context)
                                    .translate('city'),
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      ////////////////////////
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['addressAR'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ? 'العنوان':"Address (Arabic)",
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['addressEN'] = value;
                          },
                          decoration: InputDecoration(
                            labelText:Localizations.localeOf(context).languageCode =='ar' ? 'العنوان باللغة الإنكليزية':"Address (English)",
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['aboutAR'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'لمحة':'About (Arabic)',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['aboutEN'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'لمحة باللغة الإنكليزية':'About (English)',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['phoneNumber'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ? 'رقم الهاتف':'phone number',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['mobileNumber'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'رقم موبايل':'mobile number',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['email'] = value;
                          },
                          decoration: InputDecoration(
                            labelText:Localizations.localeOf(context).languageCode =='ar' ?'البريد الإلكتروني':"e-mail",
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['website'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'الموقع الإلكتروني':"website",
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['facebook'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'فيسبوك':'facebook',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          onChanged: (value) {
                            _formData['telegram'] = value;
                          },
                          decoration: InputDecoration(
                            labelText: Localizations.localeOf(context).languageCode =='ar' ?'تيليغرام':'telegram',
                            labelStyle: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: RaisedButton(
                          color: Colors.blue[300],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                          onPressed: validate,
                          child: Text(Localizations.localeOf(context).languageCode =='en'?'Submit':'إرسال'),
                        ),
                      ),
                    ],
                   ),
                   ),
                
              
            
          ],
        ),
      ),),
    );
  }
}










































