import 'package:elag_app/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:elag_app/MedicalAdverts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'package:elag_app/Model/MedicalProfile.dart';
import 'package:elag_app/Model/Media.dart';
import 'package:elag_app/Model/Research.dart';
import 'connectionmanager.dart';
import 'package:elag_app/MyDrawer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';


//import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:auto_size_text/auto_size_text.dart';

class Profile extends StatelessWidget {
  final MedicalProfile _medicalProfile;

  Profile(this._medicalProfile);

  List<Color> colorProfile;

  List<Color> getColorProfile(int id) {
    List<Color> colors = new List();
    if (id == 1) {
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    } else if (id == 2) {
      colors.add(repo.red3);
      colors.add(repo.red2);
      colors.add(repo.red1);
    } else if (id == 3) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else if (id == 4) {
      colors.add(repo.green3);
      colors.add(repo.green2);
      colors.add(repo.green1);
    } else if (id == 125) {
      colors.add(repo.tooth3);
      colors.add(repo.tooth2);
      colors.add(repo.tooth1);
    } else if (id == 97) {
      colors.add(repo.purple3);
      colors.add(repo.purple2);
      colors.add(repo.purple1);
    } else {
      // Exception
      colors.add(repo.blue3);
      colors.add(repo.blue2);
      colors.add(repo.blue1);
    }
    return colors;
  }

  @override
  Widget build(BuildContext context) {

    print(_medicalProfile.slug);
    colorProfile = getColorProfile(_medicalProfile.specialty.parentId);

    return Theme(
      data: ThemeData(
        splashColor: colorProfile[0],
        primaryColor: colorProfile[1],
        accentColor: colorProfile[1],
      ),
      child: HomeProfile(_medicalProfile, colorProfile),
    );
  }
}

class HomeProfile extends StatefulWidget {
  MedicalProfile _medicalProfile;
  List<Color> colorProfile;

  HomeProfile(this._medicalProfile, this.colorProfile);

  @override
  _HomeProfileState createState() => _HomeProfileState();
}

class _HomeProfileState extends State<HomeProfile>
    with TickerProviderStateMixin {
  TabController _tabController;
  GlobalKey<ScaffoldState> _scafoldKey = new GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    int cnt = 1;
    if (widget._medicalProfile.level == "three") cnt = 3;
   
    _tabController =
        new TabController(initialIndex: 0, length: cnt, vsync: this);
  }
 
  

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scafoldKey,
      drawer: new Opacity(
        opacity: 0.92,
        child: MyDrawer(CountryCode: Localizations.localeOf(context).languageCode,check: 0,),
      ),
      body: DefaultTabController(
        length: widget._medicalProfile.level == "three" ? 3 : 1,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: widget.colorProfile[0],
                expandedHeight: 180.0,
                floating: false,
                pinned: true,
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.arrow_forward),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ],
                flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
//                    titlePadding: EdgeInsets.only(top:1.0,bottom: 1.0),
                    title: Text(widget._medicalProfile.name==null?"":widget._medicalProfile.name,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontFamily: 'Hacen',
                        )),
                    background: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                  'images/headerProfile${widget._medicalProfile.specialty.parentId}.png'),
                              fit: BoxFit.cover,
                            ),
                            gradient:
                                LinearGradient(colors: widget.colorProfile)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
//                              padding: EdgeInsets.only(top: 32.0, right: 25.0),
                              child: CircleAvatar(
                                radius: width / 7.8,
                                backgroundImage: widget._medicalProfile.logo !=
                                        null
                                    ? NetworkImage(
                                        '$baseUrl/storage/${widget._medicalProfile.logo}')
                                    : AssetImage(
                                        'images/defaultImage.png',
                                      ),
                              ),
                            ),
                          ],
                        ))),
              ),
              SliverPersistentHeader(
                floating: true,
                delegate: _SliverAppBarDelegate(
                  widget._medicalProfile.level == 'three'
                      ? TabBar(
                          labelColor: Colors.amber,
                          unselectedLabelColor: Colors.black54,
                          indicatorColor: widget.colorProfile[2],
                          tabs: <Widget>[
                            Tab(
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('information'),
                                style: TextStyle(
                                  fontSize: 16.0,
                                  // color: Colors.black54,
                                  fontWeight: FontWeight.w900,
                                  fontFamily: 'Hacen',
                                ),
                              ),
                            ),
                            Tab(
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('gallery'),
                                style: TextStyle(
                                  fontSize: 16.0,
                                  //color: Colors.black54,
                                  fontWeight: FontWeight.w900,
                                  fontFamily: 'Hacen',
                                ),
                              ),
                            ),
                            Tab(
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('articles'),
                                style: TextStyle(
                                  fontSize: 16.0,
                                  //color: Colors.black54,
                                  fontWeight: FontWeight.w900,
                                  fontFamily: 'Hacen',
                                ),
                              ),
                            ),
                          ],
                          controller: _tabController,
                        )
                      : TabBar(
                          labelColor: Colors.amber,
                          unselectedLabelColor: Colors.black54,
                          indicatorColor: widget.colorProfile[2],
                          tabs: <Widget>[
                            Tab(
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('information'),
                                style: TextStyle(
                                  fontSize: 18.0,
                                  //color: Colors.black54,
                                  fontWeight: FontWeight.w900,
                                  fontFamily: 'Hacen',
                                ),
                              ),
                            ),
                          ],
                          controller: _tabController,
                        ),
                ),
                pinned: true,
              ),
            ];
          },
          body: widget._medicalProfile.level == "three"
              ? TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    Information(
                        widget._medicalProfile.slug, widget.colorProfile),
                    MediaProfile(widget._medicalProfile.slug),
                    Researches(widget._medicalProfile.slug),
                  ],
                )
              : TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    Information(
                        widget._medicalProfile.slug, widget.colorProfile),
                  ],
                ),
        ),
      ),
    );
  }
}

class Information extends StatefulWidget {
  final String _slug;
  List<Color> colorProfile;

  Information(this._slug, this.colorProfile);

  @override
  _InformationState createState() => _InformationState();
}

class _InformationState extends State<Information> {
  void openMap(String lat, String lng) async {
    String url = "https://www.google.com/maps/place/${lat},${lng}";
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("Could not launch");
      throw 'Could not launch Maps';
    }
  }
 
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    List<BoxShadow> listshadow = new List();
    listshadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.8,
    ));

    var profileTextStyle = new TextStyle(
      color: Colors.black,
      fontSize: 17.0,
      fontFamily: 'Hacen',
    );

    Info(MedicalProfile _medicalProfile) => Container(
          width: width,
//      height: 300.0,
          decoration: BoxDecoration(
            boxShadow: listshadow,
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    style: BorderStyle.solid,
                    width: 6.0,
                    color: widget.colorProfile[1])),
          ),
          padding: EdgeInsets.only(top: 16.0, left: 22.0, right: 22.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Name speciality
              _medicalProfile.specialty != null
                  ? Row(
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/specialistProfile.svg',
                          width: 28.0,
                          height: 28.0,
                          color: widget.colorProfile[2],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 18.0, top: 20.0, bottom: 18.0),
                        ),
                        _medicalProfile.specialty.name != null
                            ? Text(
                                _medicalProfile.specialty.name,
                                style: profileTextStyle,
                                //textDirection: TextDirection.ltr,
                              )
                            : Container(),
                      ],
                    )
                  : Container(),
              // City
//              _medicalProfile.location != null
//                  ? Row(
//                      children: <Widget>[
//                        Image.asset(
//                          'images/cityPopUp.png',
//                          width: 28.0,
//                          height: 28.0,
//                          color: widget.colorProfile[2],
//                        ),
//                        Padding(
//                          padding: EdgeInsets.only(
//                              left: 18.0, top: 20.0, bottom: 18.0),
//                        ),
//                        _medicalProfile.location.name != null
//                            ? Text(
//                                _medicalProfile.location.name,
//                                style: profileTextStyle,
//                                //textDirection: TextDirection.ltr,
//                              )
//                            : Container(),
//                      ],
//                    )
//                  : Container(),
              // Address
              _medicalProfile.address != null
                  ? Row(
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/addressProfile.svg',
                          width: 28.0,
                          height: 28.0,
                          color: widget.colorProfile[2],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 18.0, top: 20.0, bottom: 18.0),
                        ),
                        Expanded(
                          child: AutoSizeText(
                            _medicalProfile.address==null?"":_medicalProfile.address,
                            style: profileTextStyle,
                            maxLines: 2,
                            textAlign: TextAlign.end,
                            minFontSize: 2.0,
                            textDirection:Localizations.localeOf(context).languageCode =='ar'?TextDirection.ltr:TextDirection.rtl,
                          ),
                        ),
                      ],
                    )
                  : Container(),
              // Mobile
              _medicalProfile.mobile != null
                  ? Row(
                      children: <Widget>[
                        Image.asset(
                          'images/mobile.png',
                          width: 28.0,
                          height: 28.0,
                          color: widget.colorProfile[2],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 18.0, top: 20.0, bottom: 18.0),
                        ),
                        Text(
                          _medicalProfile.mobile==null?"":_medicalProfile.mobile,
                          style: profileTextStyle,
                          //textDirection: TextDirection.ltr,
                        ),
                      ],
                    )
                  : Container(),

              // Phone
              _medicalProfile.phone != null
                  ? Row(
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/teleProfiles.svg',
                          width: 28.0,
                          height: 28.0,
                          color: widget.colorProfile[2],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: 18.0, top: 20.0, bottom: 18.0),
                        ),
                        Text(
                          _medicalProfile.phone==null?"":_medicalProfile.phone,
                          style: profileTextStyle,
                          textDirection: Localizations.localeOf(context).languageCode =='ar'?TextDirection.ltr:TextDirection.rtl,
                        ),
                      ],
                    )
                  : Container(),

//              Padding(
//                padding: EdgeInsets.only(top: 4.0),
//              ),
//              Row(
//                mainAxisSize: MainAxisSize.max,
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  GestureDetector(
//                    onTap: () {
//                      print(_medicalProfile.book_appointment);
//                      if(_medicalProfile.book_appointment == null || _medicalProfile.book_appointment=="") {
//                        Scaffold.of(context).showSnackBar(SnackBar(
//                            content: Text(
//                                '${_medicalProfile
//                                    .name} غير مشترك بخدمة حجز الموعد')));
//                      }else{
//                        launch(_medicalProfile.book_appointment);
//                      }
//                    },
//                    child: Container(
//                      width: MediaQuery.of(context).size.width * 0.75,
//                      height: 35.0,
//                      decoration: BoxDecoration(
//                        border: Border.all(
//                            color: widget.colorProfile[2], width: 1.5),
//                        borderRadius: BorderRadius.circular(4.0),
//                      ),
//                      child: Center(
//                        child: Text(
//                          'احجز موعد',
//                          style: TextStyle(
//                            color: widget.colorProfile[2],
//                            fontFamily: 'Hacen',
//                            fontSize: 20.0,
//                            fontWeight: FontWeight.bold,
//                          ),
//                        ),
//                      ),
//                    ),
//                  )
//                ],
//              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _medicalProfile.website != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {
                              print(_medicalProfile.website);
                              if (await canLaunch(_medicalProfile.website)) {
                                launch(_medicalProfile.website);
                              }
                            },
                            child: Image.asset(
                              'images/icons/websiteColored.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(),
                  //Email
                  _medicalProfile.facebook != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {
                              if (await canLaunch(_medicalProfile.facebook)) {
                                launch(_medicalProfile.facebook);
                              }
                            },
                            child: Image.asset(
                              'images/icons/facebookColored.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(),
                    _medicalProfile.telegram != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {
                              if (await canLaunch(_medicalProfile.telegram)) {
                                launch(_medicalProfile.telegram);
                              }
                            },
                            child: Image.asset(
                              'images/icons/telegram.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(),
                  _medicalProfile.email != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              launch('mailto:${_medicalProfile.email}');
                            },
                            child: Image.asset(
                              'images/icons/emailColored.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(),

                  _medicalProfile.instagram != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {
                              if (await canLaunch(_medicalProfile.instagram)) {
                                launch(_medicalProfile.instagram);
                              }
                            },
                            child: Image.asset(
                              'images/icons/instagramColored.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(),

                  _medicalProfile.youtube != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {
                              if (await canLaunch(_medicalProfile.youtube)) {
                                launch(_medicalProfile.youtube);
                              }
                            },
                            child: Image.asset(
                              'images/icons/youtubeColored.png',
                              width: 28.0,
                              height: 28.0,
                            ),
                          ),
                        )
                      : Container(
                          color: Colors.red,
                        ),
                  
                  _medicalProfile.geoLocation != null
                      ? Padding(
                          padding: EdgeInsets.all(8.0),
                          child: GestureDetector(
                              onTap: () {
                                openMap(_medicalProfile.geoLocation.lat,
                                    _medicalProfile.geoLocation.lng);
                              },
                              child: Icon(Icons.location_on,size: 30, color: Colors.red,)),
                        )
                      : Container(
                          color: Colors.red,
                        ),
                ],
              ),

              Padding(
                padding: EdgeInsets.only(
                  left: 18.0,
                  top: 20.0,
                ),
              ),

              // facebook
            ],
          ),
        );
    Loc(MedicalProfile _medicalProfile) => _medicalProfile.geoLocation != null
        ? Container(
            width: width,
            height: width / 1.5,
            decoration: BoxDecoration(
              boxShadow: listshadow,
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(
                      style: BorderStyle.solid,
                      width: 6.0,
                      color: widget.colorProfile[1])),
            ),
            padding: EdgeInsets.only(
                bottom: 16.0, right: 16.0, left: 16.0, top: 6.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(flex: 30, child: MedicalAdverts()),
              ],
            ),
          )
        : Container();
    Description(String description) => description == null
        ? Container()
        : description == ''
            ? Container()
            : Container(
                width: width,
                // height: width / 1.8,
                decoration: BoxDecoration(
                  boxShadow: listshadow,
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(
                          style: BorderStyle.solid,
                          width: 6.0,
                          color: widget.colorProfile[1])),
                ),
                padding: EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      HtmlWidget(description),
//                      Text(
//                        _medicalProfile.description,
//                        style: new TextStyle(
//                          color: Colors.black54,
//                          fontSize: 17.0,
//                          fontFamily: 'Hacen',
//                          fontWeight: FontWeight.w900,
//                        ),
//                      ),
                    ],
                  ),
                ));

    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/backgroundHomePage.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        FutureBuilder<Response>(
            future: getMedicalProfile(widget._slug,Localizations.localeOf(context).languageCode), // query
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  print(snapshot.data.statusCode);
                  if (snapshot.data.statusCode == 200) {
                    MedicalProfile profile = snapshot.data.object;
                    return profile != null
                        ? SingleChildScrollView(
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Info(profile),
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                  ),
                                  Description(profile.description),
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                  ),
                                  Loc(profile),
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                  ),
                                  MedicalAdvertsSearch(
                                    profile.specialty.id.toString(),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(16.0),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container();
                  } else
                    return Center(
                      child: Text(AppLocalizations.of(context)
                          .translate('some_error_occurred')),
                    );
                } else
                  return Center(
                    child: Text('No Media'),
                  );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate('loading...'),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.black87,
                            ),
                          )),
                    ],
                  ),
                );
              }
            }),
      ],
    );
  }
}

class MediaProfile extends StatefulWidget {
  final String _slug;

  MediaProfile(this._slug);

  @override
  _MediaProfileState createState() => _MediaProfileState();
}

class _MediaProfileState extends State<MediaProfile> {
  List<Media> media;

  @override
  Widget build(BuildContext context) {
    createTile(Media med) => new GestureDetector(
          onTap: () {
            if (media != null) {
              int idx = media.indexOf(med);
              media[idx] = media[0];
              media[0] = med;
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => PhotoGallery(media)));
            }
          },
          child: Container(
            //margin: new EdgeInsets.symmetric(horizontal: 15.0),
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(3.0),
              border: Border.all(
                  color: Colors.black54, width: 0.5, style: BorderStyle.solid),
              color: Colors.amber.withOpacity(0.5),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('$baseUrl/storage/${med.path}'),
              ),
            ),
          ),
        );
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/backgroundHomePage.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        FutureBuilder<Response>(
            future: getMediaOfMedicalProfile(widget._slug,Localizations.localeOf(context).languageCode), // query
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  print(snapshot.data.statusCode);
                  print("_______________________________");
                  if (snapshot.data.statusCode == 200) {
                    media = new List();
                    media = snapshot.data.object;
                    return media.length > 0
                        ? Padding(
                            padding: EdgeInsets.only(top: 10.0, right: 6.0),
                            child: GridView.count(
                              physics: NeverScrollableScrollPhysics(),
                              crossAxisCount: 3,
                              mainAxisSpacing: 6.0,
                              crossAxisSpacing: 6.0,
                              //childAspectRatio: 9 / 16,
                              padding: EdgeInsets.all(6.0),
                              children:
                                  media.map((med) => createTile(med)).toList(),
                              shrinkWrap: true,
                            ),
                          )
                        : Container();
                  } else
                    return Center(
                      child: Text(AppLocalizations.of(context)
                          .translate('some_error_occurred')),
                    );
                } else
                  return Center(
                    child: Text('no media'),
                  );
              } else {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate('loading...'),
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.black87,
                            ),
                          )),
                    ],
                  ),
                );
              }
            }),
      ],
    );
  }
}

class Researches extends StatefulWidget {
  final String _slug;

  Researches(this._slug);

  @override
  _ResearchesState createState() => _ResearchesState();
}

class _ResearchesState extends State<Researches> {
  int page, currentPage;
  bool isLoading;
  ScrollController scrollController;
  List<Research> researches;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    researches = List();
    page = 0;
    currentPage = 0;
    isLoading = false;
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 25 &&
          researches.length % 10 == 0 &&
          !isLoading &&
          page == currentPage) {
        loadData();
      }
    });
    loadData();
  }

  void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });
      print('$page , $currentPage');
      page++;
      getResearchesOfMedicalProfile(widget._slug, 'page=${page}',Localizations.localeOf(context).languageCode)
          .then((response) {
        if (response.statusCode == 200) {
          researches.addAll(response.object);
//          if (response.object.length() > 0) {
          currentPage++;
//          }
        }
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    createTile(Research res) => new Padding(
          padding:
              EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0, bottom: 12.0),
          child: GestureDetector(
            onTap: () async {
              String url = res.url;
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                throw 'Could not launch $url';
              }
            },
            child: Container(
              width: width,
              height: 80.0,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                    color: Colors.black54,
                    width: 0.5,
                    style: BorderStyle.solid),
                color: Colors.white,
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 15.0,
                    bottom: 15.0,
                    right: 10.0,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('images/researchIcon.png'),
                              fit: BoxFit.contain)),
                      width: width / 6,
                      height: 60.0,
                    ),
                  ),
                  Positioned(
                      top: 12.0,
                      bottom: 8.0,
                      right: width / 4.0,
                      child: Text(
                        res.title,
                        style: new TextStyle(
                          color: Colors.black,
                          fontSize: 18.0,
                          fontFamily: 'Hacen',
                        ),
                      )),
                  res.author != null
                      ? Positioned(
                          bottom: 8.0,
                          left: 12.0,
                          child: Text(
                            res.author.name==null?"":res.author.name,
                            style: new TextStyle(
                              color: Colors.blue,
                              fontSize: 15.0,
                              fontFamily: 'Hacen',
                            ),
                          ))
                      : Container()
                ],
              ),
            ),
          ),
        );
    return Stack(children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/backgroundHomePage.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Padding(
          padding: EdgeInsets.only(top: 5.0),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              children: <Widget>[
                researches.length > 0
                    ? Column(
                        children: researches
                            .map((research) => createTile(research))
                            .toList())
                    : isLoading
                        ? Container()
                        : Center(
                            child: Text(AppLocalizations.of(context)
                                .translate('no_results_found')),
                          ),
                isLoading
                    ? Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircularProgressIndicator(),
                          ),
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('loading...'),
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: Colors.black87,
                                ),
                              )),
                        ],
                      )
                    : Container(),
              ],
            ),
          ))
    ]);
  }
}

class PhotoGallery extends StatelessWidget {
  List<Media> media;

  PhotoGallery(@required this.media);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(AppLocalizations.of(context).translate('gallery')),
      ),
      body: PhotoViewGallery(
        pageOptions: media.map((m) {
          return PhotoViewGalleryPageOptions(
            imageProvider: NetworkImage('$baseUrl/storage/${m.path}'),
            minScale: PhotoViewComputedScale.contained * 0.8,
            maxScale: PhotoViewComputedScale.covered * 1.1,
            //  heroTag: m.id.toString(),
          );
        }).toList(),
//           <PhotoViewGalleryPageOptions>[
//             PhotoViewGalleryPageOptions(
//               imageProvider: AssetImage("images/advert1.jpg"),
//               heroTag: "tag1",
//             ),
//             PhotoViewGalleryPageOptions(
//               imageProvider: AssetImage("images/advert1.jpg"),
//               heroTag: "tag2",
//               maxScale: PhotoViewComputedScale.contained * 0.3
//             ),
//             PhotoViewGalleryPageOptions(
//               imageProvider: AssetImage("images/advert1.jpg"),
//               minScale: PhotoViewComputedScale.contained * 0.8,
//               maxScale: PhotoViewComputedScale.covered * 1.1,
//               heroTag: "tag3",
//             ),
//           ],
//           loadingChild: widget.loadingChild,
//           backgroundDecoration: widget.backgroundDecoration,
//           pageController: widget.pageController,
//           onPageChanged: onPageChanged,
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

 
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
