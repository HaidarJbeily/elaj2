import 'package:flutter/material.dart';
import 'package:elag_app/Model/Research.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'connectionmanager.dart';
import 'package:url_launcher/url_launcher.dart';
import 'app_localizations.dart';
import 'package:auto_size_text/auto_size_text.dart';
class newestInMedicine extends StatelessWidget {
  GlobalKey<ScaffoldState> _scafoldKey;
  @override
  
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    _scafoldKey = new GlobalKey();
    List<BoxShadow> listShadow = new List();
    listShadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.1,
    ));

    List<Color> colors = new List();
    colors.add(repo.blue3);
    colors.add(repo.blue2);
    colors.add(repo.blue1);

    return Theme(
      data: new ThemeData(
        splashColor: repo.blue3,
        primaryColor: repo.blue2,
        accentColor: repo.blue2,
        inputDecorationTheme: new InputDecorationTheme(
          labelStyle: new TextStyle(
            fontSize: 15.0,
            fontFamily: 'Hacen',
          ),
        ),
      ),
      child: Scaffold(
        key: _scafoldKey,
        backgroundColor: Colors.grey[200],
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backgroundHomePage.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: width,
              height: 82.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                gradient:
                    LinearGradient(colors: colors, tileMode: TileMode.mirror),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Text(
                        AppLocalizations.of(context)
                            .translate('newest_in_medicine'),
                        style: TextStyle(
                          fontSize: 23.0,
                          //fontWeight: FontWeight.w900,
                          fontFamily: 'Hacen',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
           
            Positioned(
              top: Localizations.localeOf(context).languageCode =='ar'?27.0:27.0,
              left: Localizations.localeOf(context).languageCode =='ar'?5.0:null,
              right: Localizations.localeOf(context).languageCode =='ar'?null:5.0,
              child: GestureDetector(
                onTap: () {
                  _scafoldKey.currentState.openDrawer();
                },
                child: RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: Colors.white,
                        size: 26.0,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.pushNamed(context, '/HomePage');
                      }),
                ),
              ),
            ),

            /////////////////////
            new newestInMedicineHome(),
          ],
        ),
      ),
    );
  }
}

class newestInMedicineHome extends StatefulWidget {
  @override
  _newestInMedicineHomeState createState() => _newestInMedicineHomeState();
}

class _newestInMedicineHomeState extends State<newestInMedicineHome> {
  int page;
  int currentPage;
  List<Research> researches;

  bool isLoading;

  ScrollController scrollController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
  }

  void loadData() {
    if (page == currentPage) {
      setState(() {
        isLoading = true;
      });
      print('$page , $currentPage');
      page++;
      getResearches('page=${page}',Localizations.localeOf(context).languageCode).then((response) {
        if (response.statusCode == 200) {
          researches.addAll(response.object);
//          if (response.object.length() > 0) {
          currentPage++;
//          }
        }
        setState(() {
          isLoading = false;
        });
      });
    }
  }


  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    researches = List();
    page = 0;
    currentPage = 0;
    isLoading = false;
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter < 25 &&
          researches.length % 10 == 0 &&
          !isLoading &&
          page == currentPage) {
        loadData();
      }
    });
    loadData();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    createTile(Research res) => new Padding(
          padding:
              EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0, bottom: 12.0),
          child: GestureDetector(
            onTap: () async {
              String url = res.url;
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                throw 'Could not launch $url';
              }
            },
            child: Container(
              width: width,
              height: 80.0,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                    color: Colors.black54,
                    width: 0.5,
                    style: BorderStyle.solid),
                color: Colors.white,
              ),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('images/researchIcon.png'),
                            fit: BoxFit.contain)),
                    width: width / 6,
                    height: 60.0,
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              res.title,
                              style: new TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontFamily: 'Hacen',
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            res.publishedAt != null
                                ? Text(
                                    res.publishedAt.substring(0, 10),
                                    style: new TextStyle(
                                      color: Colors.grey,
                                      fontSize: 15.0,
                                      fontFamily: 'Hacen',
                                    ),
                                  )
                                : Container(),
                            res.author != null
                                ? AutoSizeText(
                                    res.author.name,
                                    style: new TextStyle(
                                      color: Colors.blue,
                                      fontSize: 10.0,
                                      fontFamily: 'Hacen',
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                      ],
                    ),
                  ))
                ],
              ),

            ),
          ),
        );

    return Padding(
      padding: EdgeInsets.only(top: 88.0, bottom: 12.0),
      child: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: <Widget>[
              researches.length > 0
                  ? Column(
                      children:
                          researches.map((res) => createTile(res)).toList())
                  : isLoading
                      ? Container()
                      : Center(
                          child: Text(AppLocalizations.of(context)
                              .translate('no_results_found')),
                        ),
              isLoading
                  ? Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('loading...'),
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.black87,
                              ),
                            )),
                      ],
                    )
                  : Container(),
            ],
          )),
    );
  }
}
