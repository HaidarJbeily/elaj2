
import 'package:flutter/material.dart';
import 'package:elag_app/app_localizations.dart';
import 'package:elag_app/main.dart';
import 'dart:async';
import 'package:elag_app/connectionmanager.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Dialog.dart';
import 'package:elag_app/Offers.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'package:url_launcher/url_launcher.dart';
import 'package:elag_app/newestInMedicine.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
//import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'language_constants.dart';
import 'app_localizations.dart';

class MyDrawer extends StatelessWidget {
  //GlobalKey<ScaffoldState> scafoldKey;
  String CountryCode;
  int check;
  MyDrawer({this.CountryCode,this.check});

  Future<void> requestAgain() async {
    if (Countries.length == 0) {
      var response = await getCountries(this.CountryCode);
      if (response.statusCode == 200) {
        Countries = response.object;
      }
    }

    if (Locations.length == 0) {
      var response = await getLocations(this.CountryCode);
      if (response.statusCode == 200) {
        Locations = response.object;
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Future<void> _SpecialtyPopUp(int id, List<Specialty> spec, String title,
        String imgPath, Color col, String defaultImage) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return ProfileDialog(id, spec, title, imgPath, col, defaultImage);
        },
      );
    }

    Future<void> _LocationPopUp(
        List<Location> Loc, String title, String imgPath, Color col) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new LocationDialog(Loc, title, imgPath, col);
        },
      );
    }

    Future<void> openPharmaciesPopUp(
        List<Location> Loc, String title, String imgPath, Color col) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return new OpenPharmaciesDialog(Loc, title, imgPath, col);
        },
      );
    }

    var myTextStyle = new TextStyle(
      color: Colors.black54,
      fontSize: 20.5,
      fontFamily: 'Hacen',
      fontWeight: FontWeight.w900,
    );

    var myDivider = Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Divider(
        height: 2.0,
        color: Colors.grey,
      ),
    );

    return Drawer(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          height: height / 5.0,
          width: width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/drawerHeader.png'),
              fit: BoxFit.fill,
            ),
            color: Color.fromARGB(50, 20, 180, 250),
          ),
          child: Center(
            child: Image.asset(
              'images/menu-logo.png',
              width: 100.0,
              height: 100.0,
            ),
          ),
        ),
        Container(
          height: height - (height / 10) * 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // List view of elements
              Container(
                height: height - (height / 5) - 35,
                width: width,
                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child: ListView(children: <Widget>[
                    new Padding(padding: EdgeInsets.all(2.0)),
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate("main_menu"),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/mainMenu.png',
                        width: 28.0,
                        height: 28.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/HomePage', (Route<dynamic> route) => false);
                      },
                      selected: true,
                      enabled: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate("notification"),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/Notification.png',
                        width: 28.0,
                        height: 28.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                         Navigator.of(context).pop();
                        Navigator.of(context)
                            .pushNamed(
                            '/Notification');
                      },
                      selected: true,
                      enabled: true,
                    ),
                    myDivider,
                     this.check == 1? ListTile(
                      title: new Text(
                      Localizations.localeOf(context).languageCode =="en"?  "Switch Lang.":"تغيير اللغة",
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/Ar_En.png',
                        width: 28.0,
                        height: 28.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async{
                        
                       Locations = [];
                       Countries = [];
                       Specialties = [];
                       majorSpecialties = [];
                       Adverts = [];
                      print(Countries.length);
                      print(Locations.length);
                      showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context) {
                          return WillPopScope(
                                    child:  Center(
                                        child: CircularProgressIndicator(),), 
                                        onWillPop: (){}
                                        );
                          });
                          String currentCountryCode =  Localizations.localeOf(context).languageCode;
                          Locale locale;
                          if(currentCountryCode == 'en')
                            locale = Locale('ar','KSA');
                          else
                            locale = Locale('en','US');
                            
                          MyApp.of(context).setLocale(locale);
                          await setLocale(locale.languageCode);
                          new Future.delayed(new Duration(seconds: 10), () async{
                             
                              requestAgain();
                              var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
                              if (response2.statusCode == 200) {
                                Specialties = response2.object;
                              }
                               print(Locations.length);
                              Navigator.of(context).pop();
                        Navigator.of(context)
                            .pushNamed(
                            '/HomePage');//pop dialog
                         });
                      },
                      selected: true,
                      enabled: true,
                    ):Padding(padding: EdgeInsets.zero),
                    check ==1?myDivider:Padding(padding: EdgeInsets.zero),
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('doctors'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/doctorIcon.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.of(context).pop();
                        List<Specialty> spec = new List();
                        int _id = 1;
                        requestAgain();
                        for (int i = 0; i < Specialties.length; i++) {
                          if (Specialties[i].parentId == _id) {
                            spec.add(Specialties[i]);
                          }
                        }

                        _SpecialtyPopUp(
                            1,
                            spec,
                            AppLocalizations.of(context).translate('doctors'),
                            'images/doctorPopUp.png',
                            repo.blue1,
                            'images/doctorsDefault.png');
                      },
                      selected: true,
                      enabled: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context)
                            .translate('medical_centers'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/medicalcenterIcon.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        // end request again
                        List<Specialty> spec = new List();
                        int _id = 2;
                        for (int i = 0; i < Specialties.length; i++) {
                          if (Specialties[i].parentId == _id) {
                            spec.add(Specialties[i]);
                          }
                        }
                        _SpecialtyPopUp(
                            2,
                            spec,
                            AppLocalizations.of(context)
                                .translate('medical_centers'),
                            'images/medicalCenterPopUp.png',
                            repo.red1,
                            'images/centersDefault.png');
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('pharmacies'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/pharmaciesIcon.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        // end request again
                        List<Specialty> spec = new List();
                        int _id = 4;
                        for (int i = 0; i < Specialties.length; i++) {
                          if (Specialties[i].parentId == _id) {
                            spec.add(Specialties[i]);
                          }
                        }
                        _SpecialtyPopUp(
                            4,
                            spec,
                            AppLocalizations.of(context)
                                .translate('pharmacies'),
                            'images/pharmaciesPopUp.png',
                            repo.green1,
                            'images/pharmaciesDefault.png');
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('dentists'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/dentistMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        // end request again
                        List<Specialty> spec = new List();
                        int _id = 125;
                        for (int i = 0; i < Specialties.length; i++) {
                          if (Specialties[i].parentId == _id) {
                            spec.add(Specialties[i]);
                          }
                        }
                        _SpecialtyPopUp(
                            125,
                            spec,
                            AppLocalizations.of(context).translate('dentists'),
                            'images/dentistPopUp.png',
                            repo.tooth1,
                            'images/dentistMenu.png');
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context)
                            .translate('medical_services'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/servicesMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        // end request again
                        List<Specialty> spec = new List();
                        int _id = 97;
                        for (int i = 0; i < Specialties.length; i++) {
                          if (Specialties[i].parentId == _id) {
                            spec.add(Specialties[i]);
                          }
                        }
                        _SpecialtyPopUp(
                            97,
                            spec,
                            AppLocalizations.of(context)
                                .translate('medical_services'),
                            'images/servicesPopUp.png',
                            repo.purple1,
                            'images/servicesMenu.png');
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('offers'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/offersMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => Offer()));
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context)
                            .translate('open_pharmacies'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/openPharmaciesMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        if (majorSpecialties.length == 0) {
                          var response1 = await getMajorSpecialties(Localizations.localeOf(context).languageCode);
                          if (response1.statusCode == 200) {
                            majorSpecialties = response1.object;
                          }
                        }

                        if (Specialties.length == 0) {
                          var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
                          if (response2.statusCode == 200) {
                            Specialties = response2.object;
                          }
                        }

                        // end request again
                        openPharmaciesPopUp(
                            Locations,
                            AppLocalizations.of(context).translate('city'),
                            'images/openPharmaciesMenu.png',
                            repo.tooth1);
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context)
                            .translate('medical_consultation'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/medicalConsultationMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.of(context).pop();
                        Navigator.of(context)
                            .pushNamed('/MedicalConsultationFormPage');

                        // String url = '$baseUrl/consultation';
                        // if (await canLaunch(url)) {
                        //   await launch(url);
                        // } else {
                        //   throw 'Could not launch $url';
                        // }
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('cities'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/cityIcon.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.of(context).pop();
                        // request again
                        requestAgain();
                        if (majorSpecialties.length == 0) {
                          var response1 = await getMajorSpecialties(Localizations.localeOf(context).languageCode);
                          if (response1.statusCode == 200) {
                            majorSpecialties = response1.object;
                          }
                        }

                        if (Specialties.length == 0) {
                          var response2 = await getSpecialties(Localizations.localeOf(context).languageCode);
                          if (response2.statusCode == 200) {
                            Specialties = response2.object;
                          }
                        }

                        // end request again
                        _LocationPopUp(
                            Locations,
                            AppLocalizations.of(context).translate('cities'),
                            'images/cityPopUp.png',
                            repo.yellow1);
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context)
                            .translate('newest_in_medicine'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/upToDateMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(builder: (context) => Home()));
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => newestInMedicine(),
                          ),
                        );
                      },
                      selected: true,
                    ),
//                    myDivider,
//                    new ListTile(
//                      title: new Text(
//                        'طلاب طب',
//                        style: myTextStyle,
//                      ),
//                      leading: Image.asset(
//                        'images/medicineStudents.png',
//                        width: 27.0,
//                        height: 27.0,
//                        fit: BoxFit.contain,
//                      ),
//                      onTap: () {
//                      },
//                      selected: true,
//                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('elajs_team'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/elajTeam.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {},
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('search'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/searchIcon.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(builder: (context) => Home()));
                        
                        Navigator.of(context).pop();
                        Navigator.of(context)
                            .pushNamed('/PublicSearch');
                        
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: Text(
                        AppLocalizations.of(context).translate('about_us'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/aboutAppMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(builder: (context) => Home()));
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            titlePadding: EdgeInsets.all(2.0),
                            title: Center(
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('about_us'),
                                style: TextStyle(
                                  color: repo.blue1,
                                  fontSize: 30.0,
                                  fontFamily: 'Hacen',
                                  fontWeight: FontWeight.w900,
                                ),
                              ),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: 0.0, left: 0.0, right: 0.0),
                            content: Container(
                              width: MediaQuery.of(context).size.width * 0.98,
                              height: MediaQuery.of(context).size.height * 0.94,
                              child:  SingleChildScrollView(
                                    child: Column(
                                      children: <Widget>[
                                        aboutApp == null
                                            ? Text('')
                                            : HtmlWidget(
                                                aboutApp,
                                                onTapUrl: (url) async {
                                                  if (await canLaunch(url)) {
                                                    print('cannot open $url');
                                                    await launch(url);
                                                  } else {
                                                    print('cannot open $url');
                                                  }
                                                },
                                                textStyle: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16.0,
                                                  fontFamily: 'Hacen',
                                                  fontWeight: FontWeight.w900,
                                                ),
//                                            renderNewlines: true,
                                              ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 2.0, right: 2.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            mainAxisSize: MainAxisSize.max,
                                            children: <Widget>[
                                              GestureDetector(
                                                onTap: () async {
                                                  launch(
                                                      'http://www.tinawiworld.com');
                                                },
                                                child: Container(
                                                  width: 100.0,
                                                  height: 101.0,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                'owner_company'),
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                          fontFamily: 'Hacen',
                                                          fontSize: 10.0,
                                                        ),
                                                      ),
                                                      Image.asset(
                                                        'images/dark_footer.png',
                                                        width: 50.0,
                                                        height: 50.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () async {
                                                  launch(
                                                      'http://www.zoplus.net');
                                                },
                                                child: Container(
                                                  width: 100.0,
                                                  height: 101.0,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                'programming_company'),
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                          fontFamily: 'Hacen',
                                                          fontSize: 10.0,
                                                        ),
                                                      ),
                                                      Image.asset(
                                                        'images/footer.png',
                                                        width: 50.0,
                                                        height: 50.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            ),
                          ),
                        );
                      },
                      selected: true,
                    ),
                    myDivider,
                    new ListTile(
                      title: new Text(
                        AppLocalizations.of(context).translate('join_us'),
                        style: myTextStyle,
                      ),
                      leading: Image.asset(
                        'images/subscribeMenu.png',
                        width: 27.0,
                        height: 27.0,
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.of(context).pop();
                        // Navigator.of(context).push(
                        //   MaterialPageRoute(builder: (context) => Home()));
                        Navigator.pushNamed(context, '/SubscribeFormPage');
                        // String url = '$baseUrl/join-us';
                        // if (await canLaunch(url)) {
                        //   await launch(url);
                        // } else {
                        //   throw 'Could not launch $url';
                        // }
                      },
                      selected: true,
                    ),
                    myDivider,
                  ]),
                ),
              ),

// footer
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch(
                            'https://www.youtube.com/channel/UCe2IIjCjk_TXa0aCMmBSlxg')) {
                          launch(
                              'https://www.youtube.com/channel/UCe2IIjCjk_TXa0aCMmBSlxg');
                        }
                      },
                      child: Container(
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(5.0),
                        child: Image(
                          image: AssetImage(
                            'images/icons/youtube.png',
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch(
                            'https://instagram.com/elaj.sy?igshid=1n3xomvio9q5r')) {
                          launch(
                              'https://instagram.com/elaj.sy?igshid=1n3xomvio9q5r');
                        }
                      },
                      child: Container(
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(5.0),
                        child: Image(
                          image: AssetImage(
                            'images/icons/instagram.png',
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch('https://t.me/elajsy')) {
                          launch('https://t.me/elajsy');
                        }
                      },
                      child: Container(
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(5.0),
                        child: Image(
                          image: AssetImage(
                            'images/icons/telegram.png',
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(5.0),
                        child: Image(
                          image: AssetImage(
                            'images/icons/twitter.png',
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch(
                            'https://www.facebook.com/elaajsy/?ref=br_rs')) {
                          launch('https://www.facebook.com/elaajsy/?ref=br_rs');
                        }
                      },
                      child: Container(
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(5.0),
                        child: Image(
                          image: AssetImage(
                            'images/icons/facebook.png',
                          ),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ));
  }
}
