import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:elag_app/MyDrawer.dart';
import 'Profile.dart';
import 'package:elag_app/Model/MedicalProfile.dart';
import 'package:elag_app/ColorsRepository.dart' as repo;
import 'connectionmanager.dart';
import 'app_localizations.dart';

class OpenPharmacies extends StatefulWidget {
 List<Color> _colorProfile;
  String _titleAppBar = '';
  String _titleText = '';
  String _cityId;
  String CountryCode;
  OpenPharmacies(this._cityId, this.CountryCode);

  @override
  _OpenPharmaciesState createState() => _OpenPharmaciesState();
}

class _OpenPharmaciesState extends State<OpenPharmacies> {
 
  

  void getTitle() {
    widget._titleAppBar = widget.CountryCode == 'ar'
        ? 'الصيدليات المناوبة'
        : 'Open Pharmacies';
    widget._titleText =
        widget.CountryCode == 'ar' ? 'الصيدلية' : 'Pharmacy';
  }

  List<Color> getColorProfile() {
    List<Color> colors = new List();

    colors.add(repo.green3);
    colors.add(repo.green2);
    colors.add(repo.green1);

    return colors;
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    widget._colorProfile = getColorProfile();
    getTitle();
    return HomeSearch(widget._titleAppBar, widget._titleText, widget._cityId, widget._colorProfile);
  }
}

class HomeSearch extends StatefulWidget {
  List<Color> _colorProfile;
  String _titleAppBar;
  String _titleText;
  String _cityId;

  HomeSearch(
      this._titleAppBar, this._titleText, this._cityId, this._colorProfile);

  @override
  _HomeSearchState createState() => _HomeSearchState();
}

class _HomeSearchState extends State<HomeSearch> {
  GlobalKey<ScaffoldState> _scaffoldKey;
  TextEditingController _controllerSearch;
//  Location _valDropCity;
  String _imgLeft;
  String _imgRight;
//  String _querySpecialty = '';
//  String _queryLocation = '';
//  String _queryName = '';
  String _query = '';
//  int page, currentPage;
//  List<Location> sortLocations;
  List<MedicalProfile> profiles;
  bool isLoading;
  ScrollController _scrollController;

  void buildQuery() {
    _query = 'city=${widget._cityId}';
    print(_query);
  }

  void getLeftRightImg() {
    _imgLeft = 'images/pharamSearchLeft.png';
    _imgRight = 'images/pharamSearchRight.png';
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
     _scaffoldKey = new GlobalKey();
    _controllerSearch = new TextEditingController();
    getLeftRightImg();

//    sortLocations = new List();
//
//    sortLocations.add(new Location(-1, 'اختر المدينة ..', '-1', '-1', -1, -1));
//    _valDropCity = sortLocations[0];
    profiles = List();
    isLoading = false;
    _scrollController = ScrollController();
//    _scrollController.addListener(() {
//      if (_scrollController.position.extentAfter < 25 &&
//          profiles.length % 10 == 0 &&
//          !isLoading) {
//        loadData();
//      }
//    });
    _query = 'city=${widget._cityId}';

    loadData();
  }
  void loadData() {
//    if (page == currentPage) {
    setState(() {
      isLoading = true;
    });
//      print('$page , $currentPage');
//      page++;
    getOpenPharmacies(_query,Localizations.localeOf(context).languageCode).then((response) {
      if (response.statusCode == 200) {
//          List<PharmacyShift> shifts = response.object;
//          shifts.forEach((shift){profiles.add(shift.profile);});
        profiles = response.object;

//          currentPage++;
      }
      setState(() {
        isLoading = false;
      });
    });
//    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    List<BoxShadow> listShadow = new List();
    listShadow.add(new BoxShadow(
      color: Colors.grey,
      spreadRadius: 0.1,
    ));
    var searchTextStyle = new TextStyle(
      color: Colors.black54,
      fontSize: 12.0,
      fontFamily: 'Hacen',
    );

    createTileProfile(MedicalProfile profile) => GestureDetector(
          onTap: () async {
            var pro = await getMedicalProfile(profile.slug,Localizations.localeOf(context).languageCode);
            if (pro.statusCode == 200) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Profile(pro.object)));
            }
          },
          child: Padding(
            padding:
                EdgeInsets.only(left: 15.0, right: 15.0, bottom: 6.0, top: 6.0),
            child: Container(
              height: 110.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                        color: widget._colorProfile[1],
                        style: BorderStyle.solid,
                        width: 6.0)),
              ),
              child: Stack(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: CircleAvatar(
                          radius: 40.0, //width / 8.8
                          backgroundColor: widget._colorProfile[1],
                          child: CircleAvatar(
                            radius: 38.5, //width / 9.1
                            backgroundColor: Colors.white,
                            backgroundImage: profile.logo == null
                                ? AssetImage(
                                    _imgRight,
                                  )
                                : NetworkImage(
                                    '$baseUrl/storage/${profile.logo}'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 8.0, top: 14.0, bottom: 6.0, right: 8.0),
                        child: Container(
                          width: width / 4.2,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(
                                image: AssetImage(_imgLeft),
                                fit: BoxFit.contain,
                              )),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: Text(
                      profile.name,
                      style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Hacen',
                        color: widget._colorProfile[2],
                      ),
                      //textDirection: TextDirection.rtl,
                    ),
                    top: 10.0,
                    right: 100.0, //width / 3.6
                  ),
                  Positioned(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 55.0,
                          width: width / 3.2,
                          child: Column(
                            children: <Widget>[
                              profile.specialty != null
                                  ? Row(
                                      children: <Widget>[
                                        SvgPicture.asset(
                                          'images/specialistProfile.svg',
                                          width: 20.0,
                                          height: 20.0,
                                          color: widget._colorProfile[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 8.0,
                                              bottom: 4.0,
                                              left: 14.0),
                                          child: profile.specialty.name != null
                                              ? Text(
                                                  profile.specialty.name,
                                                  style: searchTextStyle,
                                                  textDirection:
                                                      TextDirection.rtl,
                                                )
                                              : Container(),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              profile.location != null
                                  ? Row(
                                      children: <Widget>[
                                        Image.asset(
                                          'images/cityPopUp.png',
                                          width: 20.0,
                                          height: 20.0,
                                          color: widget._colorProfile[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 8.0,
                                              bottom: 4.0,
                                              left: 14.0),
                                          child: profile.location.name != null
                                              ? Text(
                                                  profile.location.name,
                                                  style: searchTextStyle,
                                                  textDirection:
                                                      TextDirection.rtl,
                                                )
                                              : Container(),
                                        ),
                                      ],
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        Container(
                          height: 55.0,
                          width: width / 3.2,
                          child: Column(
                            children: <Widget>[
                              profile.mobile != null
                                  ? Row(
                                      children: <Widget>[
                                        Image.asset(
                                          'images/mobile.png',
                                          width: 20.0,
                                          height: 20.0,
                                          color: widget._colorProfile[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 8.0, bottom: 4.0),
                                          child: Text(
                                            profile.mobile,
                                            style: searchTextStyle,
                                            textDirection: Localizations.localeOf(context).languageCode =='ar'?TextDirection.ltr:TextDirection.rtl,
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              profile.phone != null
                                  ? Row(
                                      children: <Widget>[
                                        SvgPicture.asset(
                                          'images/teleProfiles.svg',
                                          width: 20.0,
                                          height: 20.0,
                                          color: widget._colorProfile[2],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: 8.0, bottom: 4.0),
                                          child: Text(
                                            profile.phone,
                                            style: searchTextStyle,
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                            ],
                          ),
                        )
                      ],
                    ),
                    top: 45.0,
                    right: width / 3.5,
                    width: width / 1.6,
                  ),
                ],
              ),
            ),
          ),
        );

    return Theme(
      data: new ThemeData(
        splashColor: widget._colorProfile[0],
        primaryColor: widget._colorProfile[1],
        accentColor: widget._colorProfile[1],
        inputDecorationTheme: new InputDecorationTheme(
          labelStyle: new TextStyle(
            fontSize: 15.0,
            fontFamily: 'Hacen',
          ),
        ),
      ),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[200],
        drawer: new Opacity(
          opacity: 0.92,
          child: MyDrawer(),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backgroundHomePage.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              width: width,
              height: 82.0,
              decoration: BoxDecoration(
                boxShadow: listShadow,
                gradient: LinearGradient(
                    colors: widget._colorProfile, tileMode: TileMode.mirror),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 26.0),
                      child: Text(
                        widget._titleAppBar,
                        style: TextStyle(
                          fontSize: 26.5,
                          //fontWeight: FontWeight.w900,
                          fontFamily: 'Hacen',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
               top: Localizations.localeOf(context).languageCode =='en'?42.0:42.0,
              left: Localizations.localeOf(context).languageCode =='en'?16.0:null,
              right: Localizations.localeOf(context).languageCode =='en'?null:18.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: Image.asset(
                  'images/drawerIcon.png',
                  height: 18.0,
                  width: 18.0,
                ),
              ),
            ),
            Positioned(
               top: Localizations.localeOf(context).languageCode =='ar'?27.0:27.0,
              left: Localizations.localeOf(context).languageCode =='ar'?5.0:null,
              right: Localizations.localeOf(context).languageCode =='ar'?null:5.0,
              child: GestureDetector(
                onTap: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: RotatedBox(
                  quarterTurns: 2,
                  child: IconButton(
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: Colors.white,
                        size: 26.0,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 88.0),
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Column(
                  children: <Widget>[
//                    Container(
//                      width: width,
//                      height: 148.0,
//                      decoration: BoxDecoration(
//                        boxShadow: listShadow,
//                        color: Colors.white,
//                      ),
//                      child: Row(
//                        mainAxisSize: MainAxisSize.max,
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        crossAxisAlignment: CrossAxisAlignment.stretch,
//                        children: <Widget>[
//                          Container(
//                            height: 148.0,
//                            width: width / 1.56,
//                            color: Colors.white,
//                            child: Container(
//                              padding:
//                                  const EdgeInsets.only(left: 2.0, right: 5.0),
//                              child: new Column(
//                                mainAxisAlignment: MainAxisAlignment.start,
//                                crossAxisAlignment: CrossAxisAlignment.start,
//                                children: <Widget>[
//                                  Padding(
//                                    padding: EdgeInsets.all(5.0),
//                                  ),
//                                  Padding(
//                                    padding: EdgeInsets.only(
//                                        right: 12.0, left: 12.0),
//                                    child: new DropdownButton(
//                                        style: TextStyle(
//                                          fontSize: 15.0,
//                                          fontFamily: 'Hacen',
//                                          color: Colors.black,
//                                        ),
//                                        value: widget._selectedSpecialty,
////                                        icon: Image(
////                                          image: NetworkImage(
////                                              '$baseUrl/storage/${widget._selectedSpecialty.icon}'),
////                                          width: 35.0,
////                                          height: 35.0,
////                                        ),
//                                        items: widget._specialty
//                                            .map((spec) => DropdownMenuItem(
//                                                  value: spec,
//                                                  child: Text(spec.name),
//                                                ))
//                                            .toList(),
//                                        onChanged: (val) {
//                                          setState(() {
//                                            widget._selectedSpecialty = val;
//                                            _querySpecialty =
//                                                'specialty=${widget._selectedSpecialty.id},${widget._selectedSpecialty.parentId}';
//////                                            _query = _querySpecialty +
////                                                '&' +
////                                                _queryLocation;
//                                          });
//                                        }),
//                                  ),
//                                  Padding(
//                                    padding: EdgeInsets.only(
//                                      right: 12.0,
//                                      left: 12.0,
//                                    ),
//                                    child: new TextFormField(
//                                      decoration: new InputDecoration(
//                                        labelText:
//                                            "اسم " + widget._titleText + " .. ",
//                                      ),
//                                      keyboardType: TextInputType.text,
//                                      controller: _controllerSearch,
//                                      //onEditingComplete: () {
//                                      //},
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                          Container(
//                            height: 148.0,
//                            width: width / 2.79,
//                            color: Colors.white,
//                            child: Container(
//                              padding: const EdgeInsets.only(
//                                  left: 4.0, right: 8.0, bottom: 5.0),
//                              child: new Column(
//                                mainAxisAlignment: MainAxisAlignment.start,
//                                crossAxisAlignment: CrossAxisAlignment.start,
//                                children: <Widget>[
//                                  Padding(
//                                    padding: EdgeInsets.all(5.0),
//                                  ),
//                                  Padding(
//                                    padding: EdgeInsets.only(
//                                        right: 4.0, bottom: 0.0),
//                                    child: Center(
//                                      child: new DropdownButton(
//                                          //hint: Text('city'),
//                                          style: TextStyle(
//                                            fontSize: 15.5,
//                                            fontFamily: 'Hacen',
//                                            color: Colors.black,
//                                          ),
//                                          value: _valDropCity,
//                                          items: sortLocations
//                                              .map((loc) => DropdownMenuItem(
//                                                    value: loc,
//                                                    child: Text(loc.name),
//                                                  ))
//                                              .toList(),
//                                          onChanged: (val) {
//                                            setState(() {
//                                              _valDropCity = val;
//                                              if (_valDropCity.id != -1) {
//                                                if (_valDropCity.parentId !=
//                                                    null) {
//                                                  _queryLocation =
//                                                      'location=${_valDropCity.id},${_valDropCity.parentId}';
////                                                  _query = _querySpecialty +
////                                                      '&' +
////                                                      _queryLocation;
//                                                } else {
//                                                  _queryLocation =
//                                                      'location=${_valDropCity.id}';
////                                                  _query = _querySpecialty +
////                                                      '&' +
////                                                      _queryLocation;
//                                                }
//                                              } else {
////                                                _query = _querySpecialty;
//                                              }
//                                            });
//                                          }),
//                                    ),
//                                  ),
//                                  Padding(
//                                    padding: EdgeInsets.all(13.0),
//                                  ),
//                                  Padding(
//                                    padding: EdgeInsets.only(
//                                        right: 15.0, left: 15.0, bottom: 6.0),
//                                    child: new MaterialButton(
//                                      height: 35.0,
//                                      minWidth: 140.0,
//                                      onPressed: () async {
//                                        page = 1;
//                                        buildQuery();
//                                        profiles = List();
//                                        getMedicalProfiles(
//                                                '${_query}page=${page}')
//                                            .then((response) {
//                                          if (response.statusCode == 200) {
//                                            setState(() {
//                                              profiles = response.object;
//                                            });
//                                          }
//                                        });
//                                      },
//                                      textColor: Colors.white,
//                                      child: Text(
//                                        'بحث',
//                                        style: TextStyle(
//                                          fontSize: 18.0,
//                                          fontFamily: 'Hacen',
//                                          fontWeight: FontWeight.bold,
//                                        ),
//                                      ),
//                                      color: widget._colorProfile[2],
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
                    //////////////////////////////////////////////////////////

                    profiles.length > 0
                        ? Column(
                            children: profiles
                                .map((profile) => createTileProfile(profile))
                                .toList(),
                          )
                        : isLoading
                            ? Container()
                            : Center(
                                child: Text(AppLocalizations.of(context)
                                    .translate('no_results_found')),
                              ),

                    isLoading
                        ? Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              ),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate('loading...'),
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.black87,
                                    ),
                                  )),
                            ],
                          )
                        : Container(),
//                    FutureBuilder<Response>(
//                        future: getMedicalProfiles(_query), // query
//                        builder: (context, snapshot) {
//                          if (snapshot.connectionState ==
//                              ConnectionState.done) {
//                            if (snapshot.hasData) {
//                              if (snapshot.data.statusCode == 200) {
//                                List<MedicalProfile> medicalProfiles =
//                                    snapshot.data.object;
//
//                                return medicalProfiles.length > 0
//                                    ? Column(
//                                        children: medicalProfiles
//                                            .map((profile) =>
//                                                createTileProfile(profile))
//                                            .toList(),
//                                      )
//                                    : Center(
//                                        child: Text('لا توجد نتائج'),
//                                      );
//                              } else
//                                return Center(
//                                  child: Text('حدث خطأ ما'),
//                                );
//                            } else
//                              return Center(
//                                child: Text('no data'),
//                              );
//                          } else {
//                            return Column(
//                              mainAxisSize: MainAxisSize.max,
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                Padding(
//                                  padding: const EdgeInsets.all(8.0),
//                                  child: CircularProgressIndicator(),
//                                ),
//                                Padding(
//                                    padding: const EdgeInsets.all(8.0),
//                                    child: Text(
//                                      'جارٍ التحميل .. ',
//                                      style: TextStyle(
//                                        fontSize: 18.0,
//                                        color: Colors.black87,
//                                      ),
//                                    )),
//                              ],
//                            );
//                          }
//                        }),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
