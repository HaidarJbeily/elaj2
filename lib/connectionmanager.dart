import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elag_app/Model/MedicalProfile.dart';
import 'package:elag_app/Model/Media.dart';
import 'package:elag_app/Model/Research.dart';
import 'package:elag_app/Model/Location.dart';
import 'package:elag_app/Model/Specialty.dart';
import 'package:elag_app/Model/Advert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elag_app/Model/notification.dart';


//final baseUrl = 'http://209.97.156.96';
final baseUrl = 'https://elaj.care';

List<Location> Locations;

List<Location> Countries;

List<Specialty> Specialties;
List<Specialty> majorSpecialties;
List<Advert> Adverts = new List();
String aboutApp;
List <NotificationDetail> Notifications;

class Response {
  int _statusCode;
  dynamic _object;

  Response(this._statusCode, this._object);

  dynamic get object => this._object;

  set object(dynamic value) {
    this._object = value;
  }

  int get statusCode => this._statusCode;

  set statusCode(int value) {
    this._statusCode = value;
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL MEDICAL PROFILES//////////////////////////////////////
Future<Response> getMedicalProfiles(String query,String CountryCode) async {
  print('getMedicalProfiles');
  try {
    String _url =
        query == null ? '$baseUrl/api/search' : '$baseUrl/api/search?$query';
    print(_url);
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print(response.body);
    if (response.statusCode == 200) {
      print(response.body);
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<MedicalProfile> medicalProfiles =
          list.map((profile) => MedicalProfile.fromJson(profile)).toList();
      return new Response(200, medicalProfiles);
    } else {
      print('rong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print('Exception $exception');
    return new Response(-1, "error " + exception.toString());
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET OPEN PHARMACIES//////////////////////////////////////
Future<Response> getOpenPharmacies(String query,String CountryCode) async {
  try {

    String _url = '$baseUrl/api/shifts?$query';
    print(_url);
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print(response.body);
    if (response.statusCode == 200) {
      var list = jsonDecode(response.body);
      List<MedicalProfile> pharmacyShifts = List();
      list.forEach((shift) {
        MedicalProfile ps = MedicalProfile.fromJson(shift);
        pharmacyShifts.add(ps);
      });
      return new Response(200, pharmacyShifts);
    } else {
      print('rong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception);
    return new Response(-1, "error " + exception.toString());
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ABOUT APP //////////////////////////////////////
Future<Response> getAboutApp(String CountryCode) async {
  try {
    String _url = '$baseUrl/api/about';
    print(_url);
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {"x-localization": "${CountryCode}"});
    print(response.body);
    if (response.statusCode == 200) {
      String data = response.body;
      print(data);
      return new Response(200, data);
    } else {
      print('rong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception);
    return new Response(-1, "error " + exception.toString());
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET PAGE OF MEDICAL PROFILES//////////////////////////////////////
Future<List> getPageOfMedicalProfiles(int pageIndex,String CountryCode) async {
  List list = new List();
  var response = await getMedicalProfiles('page=$pageIndex',CountryCode);

  if (response.statusCode == 200) {
    list = response.object;
  }
  return list;
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET MEDICAL PROFILE//////////////////////////////////////
Future<Response> getMedicalProfile(String slug,String CountryCode) async {
  try {

Map<String,String> m  = {
      "localization":CountryCode
    };   
     http.Response response =
        await http.get(Uri.parse('$baseUrl/api/profile/$slug'),headers: {"x-localization": "${CountryCode}"});
        print("afgbsdbsdfbstestteststst");
        print('$baseUrl/api/profile/$slug');
    if (response.statusCode == 200) {
      var profile = jsonDecode(response.body);
      
      MedicalProfile medicalProfile = MedicalProfile.fromJson(profile);
      return new Response(200, medicalProfile);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception);
    return new Response(-1, "error " + exception.toString());
  }
}

Future<Response> getTest() async {
  try {
    http.Response response =
        await http.get(Uri.parse('http://209.97.156.96/api/profile/2'));

    if (response.statusCode == 200) {
      var profile = jsonDecode(response.body);
      MedicalProfile medicalProfile = MedicalProfile.fromJson(profile);
      return new Response(200, medicalProfile);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    return new Response(-1, "error " + exception.toString());
  }
}

///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET MEDIA OF MEDICAL PROFILE//////////////////////////////////////
Future<Response> getMediaOfMedicalProfile(String slug,String CountryCode) async {
  try {
    print('$baseUrl/api/profile/${slug}/media');
    var _uri = Uri.parse('$baseUrl/api/profile/${slug}/media');
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<Media> media = list.map((med) => Media.fromJson(med)).toList();
      return new Response(200, media);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET RESEARCHES OF MEDICAL PROFILE//////////////////////////////////////
Future<Response> getResearchesOfMedicalProfile(
    String slug, String query,String CountryCode) async {
  try {
    Map<String,String> m  = {
      "localization":CountryCode
    };
    http.Response response = await http
        .get(Uri.parse('$baseUrl/api/profile/${slug}/researches?$query'),headers:  {
          'x-localization': '${CountryCode}'});
    print(response.body);
    if (response.statusCode == 200) {
      List list = jsonDecode(response.body);
//      List list = data['data'];
      print('____________________________');
      List<Research> researches =
          list.map((res) => Research.fromJson(res)).toList();
      return new Response(200, researches);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL RESEARCHES//////////////////////////////////////
Future<Response> getResearches(String query, String CountryCode) async {
  try {
    String _url = query == null
        ? '$baseUrl/api/research'
        : '$baseUrl/api/research?$query';
    print('url $_url');
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print('response body ${response.body}');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<Research> researches =
          list.map((res) => Research.fromJson(res)).toList();
      //print(researches.length);
      return new Response(200, researches);
    } else {
      print('rong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    return new Response(-1, "error " + exception.toString());
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET PAGE OF RESEARCHES//////////////////////////////////////
Future<List> getPageOfResearch(int pageIndex,String CountryCode) async {
  List list = new List();
  var response = await getResearches('page=$pageIndex',CountryCode);

  if (response.statusCode == 200) {
    list = response.object;
  }
  return list;
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL Locations//////////////////////////////////////
Future<Response> getLocations(String CountryCode) async {
  try {
    http.Response response = await http.get(Uri.parse('$baseUrl/api/locations'),
        headers: {'x-localization': '${CountryCode}'});
    if (response.statusCode == 200) {
      List list = jsonDecode(response.body);
      List<Location> locations =
          list.map((loc) => Location.fromJson(loc)).toList();
      print(locations.length);
      return new Response(200, locations);
    } else {
      print('wrong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    return new Response(-1, "error " + exception.toString());
  }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL Countries//////////////////////////////////////
Future<Response> getCountries(String CountryCode) async {
  try {
    print('getting countries');
    http.Response response = await http.get(Uri.parse('$baseUrl/api/countries'),
        headers: {
          'x-localization': '${CountryCode}'
        }).timeout(Duration(seconds: 5));
    print('${response.statusCode}  ${response.body.toString()}');
    if (response.statusCode == 200) {
      List list = jsonDecode(response.body);

      List<Location> locations =
          list.map((loc) => Location.fromJson(loc)).toList();
      print(locations.length);
      return new Response(200, locations);
    } else {
      print('rong');
      return new Response(response.statusCode, response.body.toString());
    }
  } on TimeoutException catch (_) {
    print('time out');
    return new Response(-1, "timeout ");
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL Specialties//////////////////////////////////////
Future<Response> getSpecialties(String CountryCode) async {
  try {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String timeStamp = sharedPreferences.containsKey('specialitiesTS')
        ? sharedPreferences.getString('specialitiesTS')
        : '';
    String url = timeStamp != ''
        ? '$baseUrl/api/specialties'
        : '$baseUrl/api/specialties?ts=$timeStamp';
    print(url);
    var _uri = Uri.parse(url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print('response body ${response.body}');
    if (response.statusCode == 200) {
      List list = jsonDecode(response.body);
      print('new specialities length ${list.length}');
      if (list.length > 0) {
        sharedPreferences.setString(
            'specialitiesTS', DateTime.now().millisecondsSinceEpoch.toString());
      } else {
        list = List();
        if (sharedPreferences.containsKey('specialities')) {
          list = jsonDecode(sharedPreferences.getString('specialities'));
          print('old specialities length ${list.length}');
        }
      }
//      list.forEach((newSp) {
////        print('newSp id = ${newSp['id']}');
//        bool changed = false;
//        sp.forEach((oldSp) {
//          if (oldSp['id'] == newSp['id']) {
//            changed = true;
//            oldSp = newSp;
//          }
//
//        });
//        if (!changed) {
//          sp.add(newSp);
//        }
//      });
//      print('sp' + jsonEncode(sp));
//      print('list' + jsonEncode(list));

      sharedPreferences.setString('specialities', jsonEncode(list));
      List<Specialty> specialties =
          list.map((spec) => Specialty.fromJson(spec)).toList();
      print(specialties.length);
      return new Response(200, specialties);
    } else {
      print('wrong !!');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception);
    return new Response(-1, "error " + exception.toString());
  }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET Main Specialties//////////////////////////////////////
Future<Response> getMajorSpecialties(String CountryCode) async {
  try {
    http.Response response = await http
        .get(Uri.parse('$baseUrl/api/major-specialties'),
        headers: {
          'x-localization': '${CountryCode}'
        })
        .timeout(Duration(seconds: 15));
    print('${response.statusCode}  ${response.body.toString()}');
    if (response.statusCode == 200) {
      List list = jsonDecode(response.body);
      List<Specialty> specialties =
          list.map((spec) => Specialty.fromJson(spec)).toList();
      print(specialties.length);
      return new Response(200, specialties);
    } else {
      print('wrong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    return new Response(-1, "error " + exception.toString());
  }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET MEDICAL ADVERTS //////////////////////////////////////
Future<Response> getMedicalAdverts(String query,String CountryCode) async {
  try {
    String _url =
        query == null ? '$baseUrl/api/search' : '$baseUrl/api/search/$query';
    var _uri = Uri.parse(_url);
        
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<Research> researches =
          list.map((res) => Research.fromJson(res)).toList();
      //print(researches.length);
      return new Response(200, researches);
    } else {
      print('wrong');
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    return new Response(-1, "error " + exception.toString());
  }
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL Adverts//////////////////////////////////////
Future<Response> getAdverts(String query, String CountryCode) async {
  try {
    String _url =
        query == null ? '$baseUrl/api/ads' : '$baseUrl/api/ads?$query';
    print(_url);
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    if (response.statusCode == 200) {
      print('getAdverts response ${response.body}');
      List data = jsonDecode(response.body);
      List<Advert> adverts = data.map((ads) => Advert.fromJson(ads)).toList();
      return new Response(200, adverts);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////GET ALL Adverts//////////////////////////////////////
Future<Response> getOffers(String query, String CountryCode) async {
  try {
    String _url =
        query == null ? '$baseUrl/api/offers' : '$baseUrl/api/offers?$query';
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${CountryCode}'}
    );
    print('response body ${response.body}');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<Advert> adverts =
          list.map((offer) => Advert.fromJson(offer)).toList();
      return new Response(200, adverts);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}


//////////////////////////////////////////////////////////////////////////////
///////////////// GET ALL NOTIFICATION///////////////////////////////////////
///
Future<Response> getNotifications(String query,String LanguageCode) async {
  try {
    String _url =
         '$baseUrl/api/notifications?$query';
    var _uri = Uri.parse(_url);
    http.Response response = await http.get(_uri,
        headers: {
          'x-localization': '${LanguageCode}'}
    );
    print('response body ${response.body}');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      List list = data['data'];
      List<NotificationDetail> notifications =
          list.map((item) => NotificationDetail.fromJson(item)).toList();
          print("Test Notification LOAD");
          print(notifications[0].title);
      return new Response(200, notifications);
    } else {
      return new Response(response.statusCode, response.body.toString());
    }
  } catch (exception) {
    print(exception.toString());
    return new Response(-1, "error " + exception.toString());
  }
}